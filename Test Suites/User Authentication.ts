<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Verify if users can login successful.
Verify if users can logout successful.</description>
   <name>User Authentication</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>eff825f2-5ccc-4f2a-a564-b2a38b379276</testSuiteGuid>
   <testCaseLink>
      <guid>91d65014-cae4-4597-999c-f2a50284410a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC1.0 User Authentication/TC1.1 Login</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ad51fee5-b9fe-4ff7-b28b-00210ab792ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC1.0 User Authentication/TC1.2 Logout</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
