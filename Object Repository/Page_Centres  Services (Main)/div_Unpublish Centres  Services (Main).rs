<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Unpublish Centres  Services (Main)</name>
   <tag></tag>
   <elementGuidId>3df1aa29-9228-4ae7-9db1-1e0b58c1b6a0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='centres_services_main_modal_unpublish']/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#centres_services_main_modal_unpublish > div.modal-dialog.modal-dialog-centered > div.modal-content > div.modal-header</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>cc6f1561-42d9-49c9-a149-42d32dfb81e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-header</value>
      <webElementGuid>29e3e454-ea3c-4c3b-9940-b32f8f7dd049</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            Unpublish Centres &amp; Services (Main)
                            
                                ×
                            
                        </value>
      <webElementGuid>f006ba68-dc0f-4fb4-814b-36d46aafe93e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;centres_services_main_modal_unpublish&quot;)/div[@class=&quot;modal-dialog modal-dialog-centered&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-header&quot;]</value>
      <webElementGuid>67d38e8e-852c-4bcf-a1af-e3308ce5cc6c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='centres_services_main_modal_unpublish']/div/div/div</value>
      <webElementGuid>0c5cd272-5f47-4e6f-82fe-e5be70112a29</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Send For Approval'])[1]/following::div[4]</value>
      <webElementGuid>6369b484-8b74-4ac5-aa91-c9db08bd4211</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Centres &amp; Services (Main)(s)'])[1]/following::div[5]</value>
      <webElementGuid>237e41cf-6c21-4822-aee5-1ba8c4c04c71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[9]/div/div/div</value>
      <webElementGuid>7b98e74c-5d80-49de-bb5c-a8502b65d202</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                            Unpublish Centres &amp; Services (Main)
                            
                                ×
                            
                        ' or . = '
                            Unpublish Centres &amp; Services (Main)
                            
                                ×
                            
                        ')]</value>
      <webElementGuid>11ea0b20-0b96-4bf8-91ca-d072c9811c63</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
