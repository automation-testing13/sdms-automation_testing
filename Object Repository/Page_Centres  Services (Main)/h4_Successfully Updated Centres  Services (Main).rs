<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h4_Successfully Updated Centres  Services (Main)</name>
   <tag></tag>
   <elementGuidId>f1f25170-7b2a-41b8-8517-0bee9e64e32d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h4[(text() = 'Successfully Updated Centres &amp; Services (Main)' or . = 'Successfully Updated Centres &amp; Services (Main)')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h4</value>
      <webElementGuid>c945168b-3945-4fa3-bf05-70b614bd4901</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notify</name>
      <type>Main</type>
      <value>title</value>
      <webElementGuid>43e6bebf-69f6-4d84-b14c-729dd98feb67</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Successfully Updated Centres &amp; Services (Main)</value>
      <webElementGuid>c4de20f1-081c-445e-9d59-26d50156b0e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/div[@class=&quot;col-xs-12 col-sm-3 alert alert-success bg-success text-light animated fadeInDown fadeOutUp&quot;]/h4[1]</value>
      <webElementGuid>08c3a2eb-3c24-4723-8164-73c2ee283618</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h4[(text() = 'Successfully Updated Centres &amp; Services (Main)' or . = 'Successfully Updated Centres &amp; Services (Main)')]</value>
      <webElementGuid>fae54392-8e97-4c66-a6c4-2c4a00f48d59</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
