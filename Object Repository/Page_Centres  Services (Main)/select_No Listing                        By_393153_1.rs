<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_No Listing                        By_393153_1</name>
   <tag></tag>
   <elementGuidId>26fd9949-642b-45cd-a524-a3c695674704</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='centres_services_main_modal_add-select-ahp-listing-mode']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#centres_services_main_modal_add-select-ahp-listing-mode</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>6f80f827-ba9d-4450-8bc7-1a32aa93984a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control specialist-listing-mode-select</value>
      <webElementGuid>1677cf8f-a1bd-46ea-a8f2-b0fe4f41155a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>centres_services_main_modal_add-select-ahp-listing-mode</value>
      <webElementGuid>ee7ef965-3f4c-40b7-8893-57f93e369a72</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>centres_services_main_modal_add-select-ahp-listing-mode</value>
      <webElementGuid>1286dc06-a06e-4b06-b61b-512ff6075599</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>AHP Listing Mode</value>
      <webElementGuid>96293dda-8b3b-4eaa-8a18-0f60db7a2d97</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        No Listing
                        By Specialty / Service Provider Type
                        By Selection
                    </value>
      <webElementGuid>0aa101d8-43af-41d7-9b61-85c8515f875f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;centres_services_main_modal_add-select-ahp-listing-mode&quot;)</value>
      <webElementGuid>6a0fd95b-00a3-4043-a272-e3fb1f83ab54</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>84d54015-f07d-4c72-89e1-0e16c321a13a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control specialist-listing-mode-select</value>
      <webElementGuid>4337cfc0-be07-4dec-a223-26d175477ecb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>centres_services_main_modal_add-select-ahp-listing-mode</value>
      <webElementGuid>e1e11372-1fde-4304-878b-b0528d4e2296</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>centres_services_main_modal_add-select-ahp-listing-mode</value>
      <webElementGuid>a7f3646f-356a-4833-84c5-dc0a7ec8e86a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>AHP Listing Mode</value>
      <webElementGuid>0288b449-1d92-4383-8493-3db0b80a8b67</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        No Listing
                        By Specialty / Service Provider Type
                        By Selection
                    </value>
      <webElementGuid>da86cd2d-8977-447e-bca5-a7b9c2be48e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;centres_services_main_modal_add-select-ahp-listing-mode&quot;)</value>
      <webElementGuid>a7dbcda6-4c47-4d9c-b127-aaa4a68f04b2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='centres_services_main_modal_add-select-ahp-listing-mode']</value>
      <webElementGuid>ad5eb442-05e1-4221-9141-f0f68092a6da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='centres_services_main_modal_add-basic-information-tab-content']/div[2]/div[6]/div/div/select</value>
      <webElementGuid>40dfcfbf-a466-4144-ad21-1b02597b1ec2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Speech Therapy'])[2]/following::select[1]</value>
      <webElementGuid>3f590d9f-1530-468d-be2c-07d7d87f9dd4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Podiatry'])[2]/following::select[1]</value>
      <webElementGuid>f30705bc-db39-414b-89ce-acbe096b8279</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='AHP Listing Mode'])[1]/preceding::select[1]</value>
      <webElementGuid>be631089-8883-4d1e-9f47-00dde04f8626</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[7]/preceding::select[1]</value>
      <webElementGuid>73f84202-4924-419f-955a-d94d4d315ead</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/div/select</value>
      <webElementGuid>4fd86a9b-44b3-49b0-bf1c-d98bdb67f3f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'centres_services_main_modal_add-select-ahp-listing-mode' and @id = 'centres_services_main_modal_add-select-ahp-listing-mode' and @placeholder = 'AHP Listing Mode' and (text() = '
                        No Listing
                        By Specialty / Service Provider Type
                        By Selection
                    ' or . = '
                        No Listing
                        By Specialty / Service Provider Type
                        By Selection
                    ')]</value>
      <webElementGuid>701a1319-b540-44fb-8363-49bb1804e674</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
