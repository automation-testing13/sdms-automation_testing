<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Send For Approval</name>
   <tag></tag>
   <elementGuidId>703e72b8-888b-4dbd-8617-6587a9ee6c7d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='centres_services_main-button-send-approval']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#centres_services_main-button-send-approval</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>46034951-85c1-47a0-9939-4a4332721d4e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn mat-button btn-success bulk-action-button</value>
      <webElementGuid>48215340-6912-49ba-9419-4b6eb7440da8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>centres_services_main-button-send-approval</value>
      <webElementGuid>543606ef-6abe-48b4-b9f7-ceb1a251237b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>centres_services_main-button-send-approval</value>
      <webElementGuid>6a2cce51-4b36-43ec-b8a7-11f30110a847</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                    Send For Approval
                                </value>
      <webElementGuid>3b9a7959-d5a8-4f8b-9dde-94de356a4a2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;centres_services_main-button-send-approval&quot;)</value>
      <webElementGuid>2feca3d0-9987-49fc-8327-c9e6fadba008</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='centres_services_main-button-send-approval']</value>
      <webElementGuid>910381a9-110e-4732-b723-fc44700b35ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Draft'])[1]/following::button[4]</value>
      <webElementGuid>19f3a6c2-b623-4a91-9e3b-eabc26956c53</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading...'])[1]/preceding::button[1]</value>
      <webElementGuid>6828f8af-5e5b-4475-a77a-eb2833f7b395</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Centres &amp; Services (Main)'])[2]/preceding::button[1]</value>
      <webElementGuid>2ad6e2fd-b993-47a4-8ebc-c85fb7118989</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Send For Approval']/parent::*</value>
      <webElementGuid>ca740a41-1d69-458d-96bc-8a5cee0222fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]/div/button</value>
      <webElementGuid>16c1e103-8c29-4c54-ac80-6dbd1d22d8f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'centres_services_main-button-send-approval' and @name = 'centres_services_main-button-send-approval' and (text() = '
                                    
                                    Send For Approval
                                ' or . = '
                                    
                                    Send For Approval
                                ')]</value>
      <webElementGuid>7dc25648-973d-428d-945f-fe020a749803</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
