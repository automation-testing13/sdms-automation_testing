<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_No Listing                        By_393153</name>
   <tag></tag>
   <elementGuidId>26994bb6-ad62-4455-9e5c-46fe2dcc67e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='centres_services_main_modal_add-select-specialist-listing-mode']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#centres_services_main_modal_add-select-specialist-listing-mode</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>67892f2a-29ed-47b9-97bb-4f2b54b635d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control specialist-listing-mode-select</value>
      <webElementGuid>ea913021-2d77-4a27-ba3c-7c162323dd1b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>centres_services_main_modal_add-select-specialist-listing-mode</value>
      <webElementGuid>eafe8e5c-6533-4cbc-bd72-d5d7c7d6fd03</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>centres_services_main_modal_add-select-specialist-listing-mode</value>
      <webElementGuid>20c260c5-0663-4532-8196-f47778cadda2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Specialist Listing Mode</value>
      <webElementGuid>597e57cc-026b-4f2d-9c2f-2dcb0fa74ec5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        No Listing
                        By Specialty / Service Provider Type
                        By Selection
                    </value>
      <webElementGuid>69005e84-deb3-4a22-847c-cb996a1d9ba5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;centres_services_main_modal_add-select-specialist-listing-mode&quot;)</value>
      <webElementGuid>750e0cfb-d9e0-4b21-a480-c1532fd68137</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>f0e9d304-942a-45e2-b639-625374917fbd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control specialist-listing-mode-select</value>
      <webElementGuid>c70580d3-63a6-46c3-b536-a3cc068b52e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>centres_services_main_modal_add-select-specialist-listing-mode</value>
      <webElementGuid>ee5a6bf9-3da9-47f4-a6f0-db8e2ceb1979</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>centres_services_main_modal_add-select-specialist-listing-mode</value>
      <webElementGuid>6fb0f4bb-ec19-4b3a-8575-736463be0c11</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Specialist Listing Mode</value>
      <webElementGuid>0bc29932-f19b-4040-9952-181f4188b7d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        No Listing
                        By Specialty / Service Provider Type
                        By Selection
                    </value>
      <webElementGuid>202fe137-99b0-4efd-8117-9e3035a89b69</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;centres_services_main_modal_add-select-specialist-listing-mode&quot;)</value>
      <webElementGuid>42a7253f-b3bc-453b-9672-ac30a3730c98</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='centres_services_main_modal_add-select-specialist-listing-mode']</value>
      <webElementGuid>c4050226-ab4b-4236-b30a-d3c165f9e55b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='centres_services_main_modal_add-basic-information-tab-content']/div[2]/div[4]/div/div/select</value>
      <webElementGuid>92a13c27-76d6-48ff-a557-3bc26c3b760d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Done'])[1]/following::select[1]</value>
      <webElementGuid>9998cf61-c3fa-4919-978e-e6fc17d9effb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wedge Excision for Bladder Neck Contracture'])[1]/following::select[1]</value>
      <webElementGuid>cdac373e-f0df-4db0-ab03-019f445da42c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Specialist Listing Mode'])[1]/preceding::select[1]</value>
      <webElementGuid>c37d16d5-47ee-4e6f-a2ce-a2a9028d3f6c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[6]/preceding::select[1]</value>
      <webElementGuid>a146977b-4a27-46c8-ba41-d0778d026664</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/select</value>
      <webElementGuid>add1ddde-678c-4d87-bcc8-ffe43aeae4de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'centres_services_main_modal_add-select-specialist-listing-mode' and @id = 'centres_services_main_modal_add-select-specialist-listing-mode' and @placeholder = 'Specialist Listing Mode' and (text() = '
                        No Listing
                        By Specialty / Service Provider Type
                        By Selection
                    ' or . = '
                        No Listing
                        By Specialty / Service Provider Type
                        By Selection
                    ')]</value>
      <webElementGuid>fb202b1d-b545-41ec-b419-60067e7a3603</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
