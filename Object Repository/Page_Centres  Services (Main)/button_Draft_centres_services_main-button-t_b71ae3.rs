<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Draft_centres_services_main-button-t_b71ae3</name>
   <tag></tag>
   <elementGuidId>b83105c2-6cf0-4f8a-97f3-f978d0987d43</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='centres_services_main-button-translation']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#centres_services_main-button-translation</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>2d27a3bd-8c2a-4dab-b4dd-148f6e25fb26</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>9daa7f7a-a3e7-485c-98b4-9e87f91abe35</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mat-button btn-primary translate-centres-services-main-button d-flex align-items-center</value>
      <webElementGuid>7d484252-7140-468f-93fb-99ac18602c5d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>centres_services_main-button-translation</value>
      <webElementGuid>600a19ea-a7e8-4868-b6a6-936ec3efda41</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>centres_services_main-button-translation</value>
      <webElementGuid>62cac44f-ed95-4991-ae93-33818f04a966</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>modal</value>
      <webElementGuid>1dcc4a27-05df-400b-b515-f4179b0e96cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-target</name>
      <type>Main</type>
      <value>#centres_services_main_modal_translate</value>
      <webElementGuid>5bf20c37-9e0b-499d-aef5-c474d691fb56</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;centres_services_main-button-translation&quot;)</value>
      <webElementGuid>b21ef37e-f550-4edb-95a1-c97ec3bff6a1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='centres_services_main-button-translation']</value>
      <webElementGuid>e0380637-d3e1-4a30-bb1e-349bf72eaa0e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='centresServicesMainDT']/tbody/tr/td[5]/span/button</value>
      <webElementGuid>34efb972-b31f-4cf6-a9c0-5cf438e78a6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Draft'])[1]/following::button[1]</value>
      <webElementGuid>c977ffdc-b9cf-48e1-bf69-176c617db02c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Katalon Centres &amp; Services edited'])[1]/following::button[1]</value>
      <webElementGuid>04053e5e-742b-462a-9e19-d58fadf203af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Showing 1 to 1 of 1 entries (filtered from 21 total entries)'])[1]/preceding::button[3]</value>
      <webElementGuid>c04118a2-95ec-4175-8e3f-534f3ee4cab5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/button</value>
      <webElementGuid>5055d5a9-aa69-4c43-854a-c4d97d46bd05</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'centres_services_main-button-translation' and @name = 'centres_services_main-button-translation']</value>
      <webElementGuid>22da0e9b-ad2a-4ed8-a1c2-da85aa6485d9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
