<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>header_Centres  Services (Main)</name>
   <tag></tag>
   <elementGuidId>887dd91b-0895-467e-ab13-39dd3f4b51ce</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[2]/following::header[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>header.header.pt-4.px-4.d-flex.align-items-center.justify-content-between</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>header</value>
      <webElementGuid>7a7c02ba-851c-4014-b499-2ded5e756e1a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>header pt-4 px-4 d-flex align-items-center justify-content-between</value>
      <webElementGuid>c4cc756d-bce9-4851-aeb9-01bbc60dc18a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    Centres &amp; Services (Main)
                    
                        
                            
                            Add Centres &amp; Services (Main)
                        
                    
                </value>
      <webElementGuid>f8119934-5f20-47a2-851b-623b1f884ecd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;body body-open-menu&quot;]/div[@class=&quot;ic-cms&quot;]/div[@class=&quot;page-contents p-0&quot;]/div[@class=&quot;container-fluid&quot;]/header[@class=&quot;header pt-4 px-4 d-flex align-items-center justify-content-between&quot;]</value>
      <webElementGuid>6d1d7523-a16e-4af3-bf77-627ab5c6ab10</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>header</value>
      <webElementGuid>2525115d-15e2-4072-9ba9-e95a950bddb1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>header pt-4 px-4 d-flex align-items-center justify-content-between</value>
      <webElementGuid>5e1bdc83-9e6e-4b48-bc7e-53756b8def05</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    Centres &amp; Services (Main)
                    
                        
                            
                            Add Centres &amp; Services (Main)
                        
                    
                </value>
      <webElementGuid>bf85414d-7772-4699-896b-3722ebdd9f74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;body body-open-menu&quot;]/div[@class=&quot;ic-cms&quot;]/div[@class=&quot;page-contents p-0&quot;]/div[@class=&quot;container-fluid&quot;]/header[@class=&quot;header pt-4 px-4 d-flex align-items-center justify-content-between&quot;]</value>
      <webElementGuid>fa7be310-70e2-4379-a7fe-79103eaeb471</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>header</value>
      <webElementGuid>1dc947cb-24bb-41ce-9a14-f60b650d2ec2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>header pt-4 px-4 d-flex align-items-center justify-content-between</value>
      <webElementGuid>dcd99720-64eb-4518-ba3d-7b39a4a9b5ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    Centres &amp; Services (Main)
                    
                        
                            
                            Add Centres &amp; Services (Main)
                        
                    
                </value>
      <webElementGuid>0f396f97-816b-45a0-9ab8-f5e880466417</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;body body-open-menu&quot;]/div[@class=&quot;ic-cms&quot;]/div[@class=&quot;page-contents p-0&quot;]/div[@class=&quot;container-fluid&quot;]/header[@class=&quot;header pt-4 px-4 d-flex align-items-center justify-content-between&quot;]</value>
      <webElementGuid>99f71347-b19c-4148-a3b7-b6b36945f319</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>header</value>
      <webElementGuid>841d06ed-423a-46ef-a4a6-e9701ee91135</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>header pt-4 px-4 d-flex align-items-center justify-content-between</value>
      <webElementGuid>347b8f12-4748-4a3b-bb87-6a530bbbd553</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    Centres &amp; Services (Main)
                    
                        
                            
                            Add Centres &amp; Services (Main)
                        
                    
                </value>
      <webElementGuid>a89ace4b-7c86-43bb-b2e9-3f9460f7b6d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;body body-open-menu&quot;]/div[@class=&quot;ic-cms&quot;]/div[@class=&quot;page-contents p-0&quot;]/div[@class=&quot;container-fluid&quot;]/header[@class=&quot;header pt-4 px-4 d-flex align-items-center justify-content-between&quot;]</value>
      <webElementGuid>ac8ff318-26f7-4bef-8fdb-69ef6cec5bce</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[2]/following::header[1]</value>
      <webElementGuid>c052224a-19c2-45b2-9cbb-66e713f9d048</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test and Treatments'])[4]/following::header[1]</value>
      <webElementGuid>9867817f-5961-4047-a3c8-6654b11179f6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/header</value>
      <webElementGuid>e00a3dad-52e8-4127-8ed9-e61a12248399</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//header[(text() = '
                    Centres &amp; Services (Main)
                    
                        
                            
                            Add Centres &amp; Services (Main)
                        
                    
                ' or . = '
                    Centres &amp; Services (Main)
                    
                        
                            
                            Add Centres &amp; Services (Main)
                        
                    
                ')]</value>
      <webElementGuid>ce79bda6-4658-45d3-a91d-30fcc338660b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
