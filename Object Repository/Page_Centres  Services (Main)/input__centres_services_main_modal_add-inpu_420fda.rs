<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__centres_services_main_modal_add-inpu_420fda</name>
   <tag></tag>
   <elementGuidId>87b2d009-7819-4a44-aed7-f9d1573f7ea2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='centres_services_main_modal_add-input-sort-order']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#centres_services_main_modal_add-input-sort-order</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>4a52e316-3426-45c2-ad05-ff03460d64e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>2a3017fe-8e9f-4d87-91cb-1c6e456a6091</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>centres_services_main_modal_add-input-sort-order</value>
      <webElementGuid>427188cb-1094-4d5c-9acc-0720dc56cc83</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>centres_services_main_modal_add-input-sort-order</value>
      <webElementGuid>7c0521b2-18b8-4e2c-984e-34c77d9008c6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Display Order</value>
      <webElementGuid>819cdd16-50ee-4e16-8556-96a6b681065d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;centres_services_main_modal_add-input-sort-order&quot;)</value>
      <webElementGuid>fc756839-fb8f-4304-ba1a-c190a34b92f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>7f594a00-e406-4895-b74a-592922369bb6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>ab115eaa-dfbc-4ae5-9130-04b553c321c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>centres_services_main_modal_add-input-sort-order</value>
      <webElementGuid>04ca4599-e867-4f1c-ae31-33028f238a74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>centres_services_main_modal_add-input-sort-order</value>
      <webElementGuid>0e569408-a5c5-4f57-9891-1ded6c045b99</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Display Order</value>
      <webElementGuid>9f50188e-de47-4fa1-bd6d-2b73ec607113</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;centres_services_main_modal_add-input-sort-order&quot;)</value>
      <webElementGuid>a8f16db6-c06f-4e10-b289-8ebcb1d3973e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='centres_services_main_modal_add-input-sort-order']</value>
      <webElementGuid>bba0dd40-1136-4229-8047-5fa70a67d147</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='centres_services_main_modal_add-basic-information-tab-content']/div[2]/div[7]/div/div/input</value>
      <webElementGuid>54cbb1a3-0faa-40c0-a49a-7361ec2f27aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[7]/div/div/input</value>
      <webElementGuid>3c5d6bd9-81af-4603-85ca-f550de748811</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'centres_services_main_modal_add-input-sort-order' and @id = 'centres_services_main_modal_add-input-sort-order' and @placeholder = 'Display Order']</value>
      <webElementGuid>6b72b335-0a3c-477f-ba39-9052c10ce0ad</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
