<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Successfully Updated Centres  Services (Main)</name>
   <tag></tag>
   <elementGuidId>75bb239a-ee68-4ba0-b6bf-7e7391b074f7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = '    ×    Successfully Updated Centres &amp; Services (Main)' or . = '    ×    Successfully Updated Centres &amp; Services (Main)')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-3.alert.alert-success.bg-success.text-light.animated.fadeInDown</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>abea5b70-09ef-475a-adb6-6b95e6454ae4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notify</name>
      <type>Main</type>
      <value>container</value>
      <webElementGuid>5a4bac7a-5806-4fc9-94b4-82f9f4f358e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-xs-12 col-sm-3 alert alert-success bg-success text-light animated fadeInDown</value>
      <webElementGuid>18185d1d-52f3-49b3-a357-8ea11c00e20c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>alert</value>
      <webElementGuid>3586469b-fbcd-4d0d-8f44-7ca7f07bc516</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notify-position</name>
      <type>Main</type>
      <value>top-right</value>
      <webElementGuid>cbc9d56b-03d3-478b-b2db-3dd09d860635</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>    ×    Successfully Updated Centres &amp; Services (Main)</value>
      <webElementGuid>20ed6a9d-8a63-437f-a1f4-daaa904871ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;body body-open-menu&quot;]/div[@class=&quot;col-xs-12 col-sm-3 alert alert-success bg-success text-light animated fadeInDown&quot;]</value>
      <webElementGuid>21dbaa5b-d561-45e6-a5b0-bcc253c83a82</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>73758897-a381-4998-b721-b5c8c55d7ea2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notify</name>
      <type>Main</type>
      <value>container</value>
      <webElementGuid>f31e65d3-94e7-4be9-b334-6d77dc3b1d40</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-xs-12 col-sm-3 alert alert-success bg-success text-light animated fadeInDown</value>
      <webElementGuid>122de1db-75bc-4285-a6a8-2344aa91684b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>alert</value>
      <webElementGuid>275300c5-8d1c-4c0b-b2d5-dda6c3f59712</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notify-position</name>
      <type>Main</type>
      <value>top-right</value>
      <webElementGuid>2ef83f33-31d5-45f8-a116-514429f9ff5b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>    ×    Successfully Updated Centres &amp; Services (Main)</value>
      <webElementGuid>6b5c93ba-b6b6-4561-b82c-3b1794ca76c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;body body-open-menu&quot;]/div[@class=&quot;col-xs-12 col-sm-3 alert alert-success bg-success text-light animated fadeInDown&quot;]</value>
      <webElementGuid>770f728b-597f-4467-8fa5-5cea3bc4114a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>389723cf-5515-41a1-bb76-889ceb796e35</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notify</name>
      <type>Main</type>
      <value>container</value>
      <webElementGuid>afe08f66-21c8-4e4e-aa8c-07501eb9321f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-xs-12 col-sm-3 alert alert-success bg-success text-light animated fadeInDown</value>
      <webElementGuid>3ce7a471-baea-4633-a3ce-f00a88632e52</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>alert</value>
      <webElementGuid>c957a1f0-877e-4085-8c64-ef3abdda8264</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notify-position</name>
      <type>Main</type>
      <value>top-right</value>
      <webElementGuid>9f532e83-67ca-48ee-8b77-4e580dde6a7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>    ×    Successfully Updated Centres &amp; Services (Main)</value>
      <webElementGuid>d501da8c-4048-46f2-ab96-4151ef7c5f61</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;body body-open-menu&quot;]/div[@class=&quot;col-xs-12 col-sm-3 alert alert-success bg-success text-light animated fadeInDown&quot;]</value>
      <webElementGuid>5bb43eb7-2785-47b5-ae0d-7911cda484fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>4403180d-4987-43e5-bbb9-bd7fa3502a33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notify</name>
      <type>Main</type>
      <value>container</value>
      <webElementGuid>4d790df5-f69b-4edf-9012-af1bc4a476df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-xs-12 col-sm-3 alert alert-success bg-success text-light animated fadeInDown</value>
      <webElementGuid>315008d2-d416-4786-bffb-2a0cbd065b15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>alert</value>
      <webElementGuid>fac07cd3-6c0e-4e86-867f-6473604f0c76</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notify-position</name>
      <type>Main</type>
      <value>top-right</value>
      <webElementGuid>9c83f66b-920f-4594-90b0-5a89d63f8fc6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>    ×    Successfully Updated Centres &amp; Services (Main)</value>
      <webElementGuid>bb6208ea-f65f-4128-a0f7-e820fdec295d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;body body-open-menu&quot;]/div[@class=&quot;col-xs-12 col-sm-3 alert alert-success bg-success text-light animated fadeInDown&quot;]</value>
      <webElementGuid>f8772347-38d5-46df-b79b-6caf433b1ed8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[2]</value>
      <webElementGuid>7fd33bf9-008e-4ea3-8394-bc798843e240</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[2]/following::div[2]</value>
      <webElementGuid>6684db5e-caf8-4861-954a-73e22e81f666</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]</value>
      <webElementGuid>6788fdce-cdfc-4995-b27b-a93b40f74d1c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '    ×    Successfully Updated Centres &amp; Services (Main)' or . = '    ×    Successfully Updated Centres &amp; Services (Main)')]</value>
      <webElementGuid>f5e03cc5-bfe3-4449-b10e-e8a71f85113e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
