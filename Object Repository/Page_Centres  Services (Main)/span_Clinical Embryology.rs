<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Clinical Embryology</name>
   <tag></tag>
   <elementGuidId>2cc62d2d-21b9-487e-a4c6-b8438c9ce6f9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Deselect All'])[2]/following::span[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#bs-select-2-0 > span.text</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>7253bd83-2d98-473c-b0b0-6e38416ee11b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>add3f333-dfa2-45e6-90e7-30d0c656316d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Clinical Embryology</value>
      <webElementGuid>fd593155-5c19-4536-8216-09df31de3579</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;bs-select-2-0&quot;)/span[@class=&quot;text&quot;]</value>
      <webElementGuid>8a43d9f3-02fb-475c-b7b4-cdbc27d01e12</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='bs-select-2-0']/span[2]</value>
      <webElementGuid>d2d62243-a9b2-4bf3-9455-810383012d88</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deselect All'])[2]/following::span[2]</value>
      <webElementGuid>d43a6b72-a4b7-4cd1-8fd1-92b860f742bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select All'])[2]/following::span[2]</value>
      <webElementGuid>24db2c6d-b634-4232-ba7f-cfaad52ed719</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Medical Advisor'])[2]/preceding::span[2]</value>
      <webElementGuid>eb4b1c4c-0168-43f7-a941-959564e88379</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nutrition and Dieteticsa'])[2]/preceding::span[4]</value>
      <webElementGuid>d109dd98-8375-4bf9-aabe-ad86589e701e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div/div/div[2]/ul/li/a/span[2]</value>
      <webElementGuid>2b08fc24-0947-4a70-ad2d-fd10bbdc86fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Clinical Embryology' or . = 'Clinical Embryology')]</value>
      <webElementGuid>cb1bf92a-91b8-403a-8ea3-49b46df0136b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
