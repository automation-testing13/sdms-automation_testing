<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>header_Welcome</name>
   <tag></tag>
   <elementGuidId>34442676-b5bf-4e3b-91f9-5e1ca5d4c1f7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='STRUCTURED DATA MANAGEMENT SYSTEM'])[1]/following::header[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>header.topbar.box-shadow-card.z-index-1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>header</value>
      <webElementGuid>c1a9a879-8fde-4fb8-b916-dd48b53c2f60</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>topbar box-shadow-card z-index-1</value>
      <webElementGuid>23ddd7bc-eac7-435f-abe3-15cb1ee6ca65</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
        
            
                
                    
                    
                
                Welcome umAdmin
            
            
                
                    
                        
                            
                            
                        
                        
                            
                            umAdmin
                        
                    
                
                
                    
                    Logout
                
            
        
    
</value>
      <webElementGuid>0c1f4987-ff05-46f2-99f8-93557c0910c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;body body-open-menu&quot;]/div[@class=&quot;ic-cms&quot;]/header[@class=&quot;topbar box-shadow-card z-index-1&quot;]</value>
      <webElementGuid>507d04a1-c795-4793-a802-066423cc607e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='STRUCTURED DATA MANAGEMENT SYSTEM'])[1]/following::header[1]</value>
      <webElementGuid>154d3ba1-4657-4d81-b4e6-cd374875831c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//header</value>
      <webElementGuid>b6b3d294-09de-4561-a29d-09c4a4dde90e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//header[(text() = '
    
        
        
            
                
                    
                    
                
                Welcome umAdmin
            
            
                
                    
                        
                            
                            
                        
                        
                            
                            umAdmin
                        
                    
                
                
                    
                    Logout
                
            
        
    
' or . = '
    
        
        
            
                
                    
                    
                
                Welcome umAdmin
            
            
                
                    
                        
                            
                            
                        
                        
                            
                            umAdmin
                        
                    
                
                
                    
                    Logout
                
            
        
    
')]</value>
      <webElementGuid>ae76c8c8-f0c8-408e-ab48-b714e2cb9f5d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
