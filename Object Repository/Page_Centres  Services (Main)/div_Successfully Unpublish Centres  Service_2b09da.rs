<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Successfully Unpublish Centres  Service_2b09da</name>
   <tag></tag>
   <elementGuidId>6ee5648a-4fbd-48fa-b026-b224898859dc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = '    ×    Successfully Unpublish Centres &amp; Services (Main)' or . = '    ×    Successfully Unpublish Centres &amp; Services (Main)')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c4f7fe3b-e23d-4532-86da-b262ebde827a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notify</name>
      <type>Main</type>
      <value>container</value>
      <webElementGuid>8a3c1926-1a4d-483e-9f1c-c2ec9c852216</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-xs-12 col-sm-3 alert alert-success bg-success text-light animated fadeInDown fadeOutUp</value>
      <webElementGuid>ad4c278b-7d5f-4fd2-b67d-c915798a1203</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>alert</value>
      <webElementGuid>415b9fc6-5d1d-45bb-8827-40348e3c2022</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notify-position</name>
      <type>Main</type>
      <value>top-right</value>
      <webElementGuid>0b3c6e86-a0e4-48e4-ab23-0dbb6d08787a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>    ×    Successfully Unpublish Centres &amp; Services (Main)</value>
      <webElementGuid>5bbfd70c-92eb-407c-b88c-2d5129efaf8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/div[@class=&quot;col-xs-12 col-sm-3 alert alert-success bg-success text-light animated fadeInDown fadeOutUp&quot;]</value>
      <webElementGuid>1c49789b-8dcb-455c-adbe-1ec88a6b036d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '    ×    Successfully Unpublish Centres &amp; Services (Main)' or . = '    ×    Successfully Unpublish Centres &amp; Services (Main)')]</value>
      <webElementGuid>a77b7793-4dc1-4cfc-a1ca-67f563df1912</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
