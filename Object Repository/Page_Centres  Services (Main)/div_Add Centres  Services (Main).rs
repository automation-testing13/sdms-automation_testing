<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Add Centres  Services (Main)</name>
   <tag></tag>
   <elementGuidId>0860c8dd-3e81-41cb-b089-8b3f0195001b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='centres_services_main_modal_add']/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.modal-header</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>2dd61ea3-b02a-4713-9026-2f7fac5763e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-header</value>
      <webElementGuid>30226db0-652f-41e0-a247-4405fd463587</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Add Centres &amp; Services (Main)
                
                    ×
                
            </value>
      <webElementGuid>7fba2c28-6436-43f9-b548-9b3bd9467d41</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;centres_services_main_modal_add&quot;)/div[@class=&quot;modal-dialog modal-dialog-centered modal-xl&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-header&quot;]</value>
      <webElementGuid>4db9c614-e1d8-4f1b-9bd6-c22158c48c08</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='centres_services_main_modal_add']/div/div/div</value>
      <webElementGuid>aa17d5bc-c47f-4f2c-aeca-12d38f1e3b5e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading...'])[1]/following::div[4]</value>
      <webElementGuid>47a394e7-c92a-41a5-af5e-e4298d7c9bc5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Basic Information'])[1]/preceding::div[1]</value>
      <webElementGuid>1b0ffd00-bcaf-4852-ac7a-d59046de2469</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div</value>
      <webElementGuid>fd13b62b-e18d-483c-9422-8fd3674384ba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                Add Centres &amp; Services (Main)
                
                    ×
                
            ' or . = '
                Add Centres &amp; Services (Main)
                
                    ×
                
            ')]</value>
      <webElementGuid>361e28aa-02e7-4021-894b-263ca78fa345</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
