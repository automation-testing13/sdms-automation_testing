<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h4_Successfully send Centres  Services (Mai_7b5c15</name>
   <tag></tag>
   <elementGuidId>d74d055a-e6b0-4585-aa90-a5f8d3bbe553</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Apply'])[2]/following::h4[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-3.alert.alert-success.bg-success.text-light.animated.fadeInDown > h4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h4</value>
      <webElementGuid>e68a1ded-b4aa-4664-b425-85798e8a7f32</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notify</name>
      <type>Main</type>
      <value>title</value>
      <webElementGuid>149f365b-56e2-49d6-b8f3-5039b3641cd6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Successfully send Centres &amp; Services (Main) for approval</value>
      <webElementGuid>55aa4e4d-77a8-476d-8a44-2d10c9507111</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;body body-open-menu&quot;]/div[@class=&quot;col-xs-12 col-sm-3 alert alert-success bg-success text-light animated fadeInDown&quot;]/h4[1]</value>
      <webElementGuid>0d1b235b-8405-4a70-a60f-87508deadbd8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apply'])[2]/following::h4[1]</value>
      <webElementGuid>6f2c2d88-15a6-41c4-bf03-ee0f5fc09acd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[5]/following::h4[1]</value>
      <webElementGuid>37494ccf-e0ed-471a-ab52-e3b25e78c962</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Successfully send Centres &amp; Services (Main) for approval']/parent::*</value>
      <webElementGuid>460e6fd3-4599-4734-b534-b3cf84ad9ff3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/h4</value>
      <webElementGuid>ddd95b8b-f0a5-4a8b-b312-62d01fd0e7fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h4[(text() = 'Successfully send Centres &amp; Services (Main) for approval' or . = 'Successfully send Centres &amp; Services (Main) for approval')]</value>
      <webElementGuid>11075e04-437b-48c2-a68b-c2dff4b91410</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
