<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Centres  Services (Main)               _81135e</name>
   <tag></tag>
   <elementGuidId>1e03bb77-876f-4432-9e48-70e73829f4e5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='centres_services_main_modal_translate']/div/div/div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#centres_services_main_modal_translate > div.modal-dialog.modal-dialog-centered.modal-xl > div.modal-content > div.modal-header</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>d655542a-f1a7-4034-b008-5cbb729026c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-header</value>
      <webElementGuid>8c82ff87-1df6-46a5-b2f7-5c94ca0a38c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Centres &amp; Services (Main)
                
                    
                        Bahasa IndonesiaChineseEnglishVietnamese
                        
                    
                
                
                    ×
                
            </value>
      <webElementGuid>7f496b1e-ff0a-4e57-a6ef-0ac22005876e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;centres_services_main_modal_translate&quot;)/div[@class=&quot;modal-dialog modal-dialog-centered modal-xl&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-header&quot;]</value>
      <webElementGuid>784ab4c7-f46b-442f-be67-d421fb813673</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='centres_services_main_modal_translate']/div/div/div[2]</value>
      <webElementGuid>55ea1e0e-425d-462c-b265-02e29ea215a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sending item for translation'])[1]/following::div[1]</value>
      <webElementGuid>1ab619a1-a01c-49b8-ae76-daf8af3f7c4c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save and Close'])[1]/following::div[6]</value>
      <webElementGuid>af800bb8-8a32-4e18-9108-f07b17efd109</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Basic Information'])[3]/preceding::div[3]</value>
      <webElementGuid>76ad5124-b129-41f4-86f6-98274e9a8bf6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/div/div[2]</value>
      <webElementGuid>81287cf4-7f24-412c-8945-1c9aa5989984</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                Centres &amp; Services (Main)
                
                    
                        Bahasa IndonesiaChineseEnglishVietnamese
                        
                    
                
                
                    ×
                
            ' or . = '
                Centres &amp; Services (Main)
                
                    
                        Bahasa IndonesiaChineseEnglishVietnamese
                        
                    
                
                
                    ×
                
            ')]</value>
      <webElementGuid>a82583b8-a27b-4e28-9b94-ae17956cf6f6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
