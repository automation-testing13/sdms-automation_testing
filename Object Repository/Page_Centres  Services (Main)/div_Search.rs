<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Search</name>
   <tag></tag>
   <elementGuidId>65fe6d15-c2a4-4bf4-8ba2-9181ec97c515</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Centres &amp; Services (Main)'])[1]/following::div[4]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.mat-div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3c83af53-e934-4892-aa64-f04174325392</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mat-div</value>
      <webElementGuid>572f9b15-69f9-463f-ae4a-63bcd8c580c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                Search
                                
                            </value>
      <webElementGuid>d5c6ab99-8290-44d0-bad4-527d2bca3e1f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;body body-open-menu&quot;]/div[@class=&quot;ic-cms&quot;]/div[@class=&quot;page-contents p-0&quot;]/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;content-container&quot;]/div[@class=&quot;row mb-4&quot;]/div[@class=&quot;col-md-8&quot;]/div[@class=&quot;mat-div&quot;]</value>
      <webElementGuid>8e096448-f4e1-44ea-85b7-b092a247479b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Centres &amp; Services (Main)'])[1]/following::div[4]</value>
      <webElementGuid>d24db5cb-6561-4e65-a2b1-49ee49181e9c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Centres &amp; Services (Main)'])[4]/following::div[4]</value>
      <webElementGuid>ef79fede-52fd-4889-b290-42047581f53b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search'])[2]/preceding::div[1]</value>
      <webElementGuid>d6bbe3df-8c50-4222-bd31-a846a325bcb2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div</value>
      <webElementGuid>6efb834e-95e2-4472-ae32-21032a7026c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                Search
                                
                            ' or . = '
                                Search
                                
                            ')]</value>
      <webElementGuid>c107d734-2fb1-44eb-9e17-b448c763b758</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
