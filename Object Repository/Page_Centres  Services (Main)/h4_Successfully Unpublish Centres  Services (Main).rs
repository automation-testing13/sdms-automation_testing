<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h4_Successfully Unpublish Centres  Services (Main)</name>
   <tag></tag>
   <elementGuidId>3b5b4a10-a2de-45d8-bcf9-a4caccec47a1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Apply'])[2]/following::h4[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-sm-3.alert.alert-success.bg-success.text-light.animated.fadeInDown.fadeOutUp > h4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h4</value>
      <webElementGuid>3ad898fb-042d-4046-9c0f-67e2ba331912</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notify</name>
      <type>Main</type>
      <value>title</value>
      <webElementGuid>4ddac95d-aebe-41c7-84da-9b015f38faa1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Successfully Unpublish Centres &amp; Services (Main)</value>
      <webElementGuid>c8ac017d-5325-44ae-9633-800f0979770a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;body body-open-menu&quot;]/div[@class=&quot;col-xs-12 col-sm-3 alert alert-success bg-success text-light animated fadeInDown fadeOutUp&quot;]/h4[1]</value>
      <webElementGuid>b61aefb8-e593-4e21-b4be-a34596f75361</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apply'])[2]/following::h4[1]</value>
      <webElementGuid>83868ddc-9d6b-4794-a941-86dfcad7b51a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[5]/following::h4[1]</value>
      <webElementGuid>95892b49-88fc-47c9-9d17-9349fa61dd6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Successfully Unpublish Centres &amp; Services (Main)']/parent::*</value>
      <webElementGuid>d8d5c710-68e7-42d6-95f2-8fe8729fd426</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/h4</value>
      <webElementGuid>77d76f6f-31be-4ff6-9838-df15fb696f3d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h4[(text() = 'Successfully Unpublish Centres &amp; Services (Main)' or . = 'Successfully Unpublish Centres &amp; Services (Main)')]</value>
      <webElementGuid>32e16e86-dc31-4155-882a-8cf18b927dfe</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
