<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>centres_services_main_modal_add-select-ahp-listing-mode</name>
   <tag></tag>
   <elementGuidId>2c0bc7a2-8992-4913-9a15-e3c8e5aa1e36</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='centres_services_main_modal_add-select-ahp-listing-mode']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#centres_services_main_modal_add-select-ahp-listing-mode</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>7bf3035b-c5e4-48bc-a4fe-72f9617045f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control specialist-listing-mode-select</value>
      <webElementGuid>a565e631-2dca-4955-b630-127e50fd53ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>centres_services_main_modal_add-select-ahp-listing-mode</value>
      <webElementGuid>d519e3bc-38cf-448b-bb69-8d251240ac3a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>centres_services_main_modal_add-select-ahp-listing-mode</value>
      <webElementGuid>4765ac1e-5a85-4986-80b7-14f1600c6dfe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>AHP Listing Mode</value>
      <webElementGuid>84c06e2e-d188-48c2-9a12-794fb1a7ac2e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        No Listing
                        By Specialty / Service Provider Type
                        By Selection
                    </value>
      <webElementGuid>6cbc9cc5-3284-40d7-a589-dba87d1ea1b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;centres_services_main_modal_add-select-ahp-listing-mode&quot;)</value>
      <webElementGuid>c412b7af-2dd0-4226-a0c1-7a636c54ce30</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>317418c4-204a-4665-9496-37a8689859e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control specialist-listing-mode-select</value>
      <webElementGuid>9b75d021-05f9-4ed9-b08a-804dcffdf2dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>centres_services_main_modal_add-select-ahp-listing-mode</value>
      <webElementGuid>d8d134a2-e047-42f7-8ea2-3030f029bbea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>centres_services_main_modal_add-select-ahp-listing-mode</value>
      <webElementGuid>6c4bfb78-e633-4298-8b82-addbc943fa1a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>AHP Listing Mode</value>
      <webElementGuid>eb1dfa03-d97c-4edb-be06-c20ad49400a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        No Listing
                        By Specialty / Service Provider Type
                        By Selection
                    </value>
      <webElementGuid>f7909cde-bd95-411c-bc81-4bc1bc493b05</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;centres_services_main_modal_add-select-ahp-listing-mode&quot;)</value>
      <webElementGuid>7126fb74-16bd-4bb7-a2a7-f9785c1d5e72</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>24c0e683-d0d2-4168-b720-4c39c063ed6d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control specialist-listing-mode-select</value>
      <webElementGuid>017fb184-4ab2-4483-803f-d94d01aa04df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>centres_services_main_modal_add-select-ahp-listing-mode</value>
      <webElementGuid>87c56bb6-eb65-41d1-8001-c002dab70003</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>centres_services_main_modal_add-select-ahp-listing-mode</value>
      <webElementGuid>69d77ccb-ca4f-475d-8766-3ece910bab72</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>AHP Listing Mode</value>
      <webElementGuid>0ef0e214-7f1e-4b4d-8be6-fa7e8b37f1d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        No Listing
                        By Specialty / Service Provider Type
                        By Selection
                    </value>
      <webElementGuid>d669603c-0b7d-4323-bc28-da25cde04bfd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;centres_services_main_modal_add-select-ahp-listing-mode&quot;)</value>
      <webElementGuid>c8d5d54e-d49f-41d5-b0a5-2a1c34e337ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>94094089-2f02-474b-9458-c847cdc9a67e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control specialist-listing-mode-select</value>
      <webElementGuid>63bb2f98-7973-45f1-965b-717c16a5019a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>centres_services_main_modal_add-select-ahp-listing-mode</value>
      <webElementGuid>a20656b2-37bf-4ee6-bb3c-0c288b7eb6cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>centres_services_main_modal_add-select-ahp-listing-mode</value>
      <webElementGuid>e7a4b5a1-1ed9-4b94-bc00-c8dab1da448e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>AHP Listing Mode</value>
      <webElementGuid>14233e8a-ee03-496e-b63b-c719097cba1d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        No Listing
                        By Specialty / Service Provider Type
                        By Selection
                    </value>
      <webElementGuid>0a84f611-44b1-44c2-a278-4d8904f37991</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;centres_services_main_modal_add-select-ahp-listing-mode&quot;)</value>
      <webElementGuid>4d08c558-b9f9-48e0-8391-9fdd92bc047e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='centres_services_main_modal_add-select-ahp-listing-mode']</value>
      <webElementGuid>ab780628-8cc0-4d88-933b-1138567b6f06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='centres_services_main_modal_add-basic-information-tab-content']/div[2]/div[6]/div/div/select</value>
      <webElementGuid>1f119a4e-1cde-4186-8f43-d1eb925b8b20</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Speech Therapy'])[2]/following::select[1]</value>
      <webElementGuid>f0711e1b-9534-4dd8-aaa1-fc3a20bf3c7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Podiatry'])[2]/following::select[1]</value>
      <webElementGuid>fd9b5d0c-c4f3-493c-8a4e-ed2ccf05a84f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='AHP Listing Mode'])[1]/preceding::select[1]</value>
      <webElementGuid>409dd3e5-7151-49c7-8af2-cd01db2555fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[7]/preceding::select[1]</value>
      <webElementGuid>1519064f-6d5f-4649-9072-853342ac0696</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/div/select</value>
      <webElementGuid>233f3a64-2e96-447d-87b8-d8d8578f393c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'centres_services_main_modal_add-select-ahp-listing-mode' and @id = 'centres_services_main_modal_add-select-ahp-listing-mode' and @placeholder = 'AHP Listing Mode' and (text() = '
                        No Listing
                        By Specialty / Service Provider Type
                        By Selection
                    ' or . = '
                        No Listing
                        By Specialty / Service Provider Type
                        By Selection
                    ')]</value>
      <webElementGuid>1ff005cc-6f62-4236-9918-5f5827d8ba04</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
