<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Delete Centres  Services (Main)</name>
   <tag></tag>
   <elementGuidId>0f0a6191-b500-4597-9c00-11586c5a920b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='delete-centres-services-main-modal']/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#delete-centres-services-main-modal > div.modal-dialog.modal-dialog-centered > div.modal-content > div.modal-header</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>be198518-5dbd-4b31-92ec-c81ad2eac847</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-header</value>
      <webElementGuid>5fcf9723-cfd1-46ac-a44d-ca495be1e016</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            Delete Centres &amp; Services (Main)
                            
                                ×
                            
                        </value>
      <webElementGuid>8fa6ce61-6af5-465f-ae58-2c7b5a5fe84f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;delete-centres-services-main-modal&quot;)/div[@class=&quot;modal-dialog modal-dialog-centered&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-header&quot;]</value>
      <webElementGuid>b7060f09-42a3-4b43-b46b-0aea8c34195c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='delete-centres-services-main-modal']/div/div/div</value>
      <webElementGuid>20fcdc22-7f25-413b-ac07-463e07fa3e87</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Send for Translation'])[1]/following::div[4]</value>
      <webElementGuid>85eff74f-8617-41f4-9304-6778931fe9ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Choose Translation Workflow'])[1]/following::div[4]</value>
      <webElementGuid>157c98e2-77e2-424d-9088-7d0a6671475e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div/div</value>
      <webElementGuid>f19c7f85-136e-4627-9ea7-e6a74948655b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                            Delete Centres &amp; Services (Main)
                            
                                ×
                            
                        ' or . = '
                            Delete Centres &amp; Services (Main)
                            
                                ×
                            
                        ')]</value>
      <webElementGuid>9c148e72-1ffc-442d-9ad5-1722356191aa</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
