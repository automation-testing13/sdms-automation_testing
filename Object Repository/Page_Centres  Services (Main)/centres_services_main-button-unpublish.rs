<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>centres_services_main-button-unpublish</name>
   <tag></tag>
   <elementGuidId>47937c78-90a3-4cd1-a3fa-8146562caa19</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='centres_services_main-button-unpublish']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#centres_services_main-button-unpublish</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>08b629e7-93d0-4fe8-b119-d7c66421cf8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>9094c12d-e991-49a8-8a0b-0735895fc405</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mat-button btn-only-icon btn-secondary unpublish-centres-services-main-button</value>
      <webElementGuid>95204267-e03f-4343-ade8-97fd3cc3bf15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>centres_services_main-button-unpublish</value>
      <webElementGuid>6587a8dc-d20f-477f-ae13-b52a790234f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>centres_services_main-button-unpublish</value>
      <webElementGuid>a14bcaea-b640-4c13-971c-827b6a00c46d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>modal</value>
      <webElementGuid>6819838f-1aa8-4763-9f3e-d5993a04e578</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-target</name>
      <type>Main</type>
      <value>#centres_services_main_modal_unpublish</value>
      <webElementGuid>a99ce0de-cfab-4762-a366-54744d77195d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;centres_services_main-button-unpublish&quot;)</value>
      <webElementGuid>ec3150ac-0fa9-41a6-a854-623fac210349</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='centres_services_main-button-unpublish']</value>
      <webElementGuid>4d8c11ea-f778-4893-8f12-3ee6f1ca4072</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='centresServicesMainDT']/tbody/tr/td[6]/span[2]/button</value>
      <webElementGuid>e23edacc-f6e1-4140-b56c-dbcc8adaf8be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Published'])[1]/following::button[3]</value>
      <webElementGuid>2efc5d94-285c-4648-9b6f-12196c839f25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Katalon Centres &amp; Services edited'])[1]/following::button[3]</value>
      <webElementGuid>8844978b-d53a-4ea0-9565-8a63eee966da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Showing 1 to 1 of 1 entries (filtered from 21 total entries)'])[1]/preceding::button[1]</value>
      <webElementGuid>d92331d0-cb06-4682-b9ec-2b4b3e44c138</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]/button</value>
      <webElementGuid>e346598e-fe8a-4573-93d5-47b00ca6721c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'centres_services_main-button-unpublish' and @name = 'centres_services_main-button-unpublish']</value>
      <webElementGuid>1bc2cd9d-cfe8-43a4-8429-de6f1ddb29a9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
