<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Edit Centres  Services (Main)          _24fcef</name>
   <tag></tag>
   <elementGuidId>d074cc7d-b475-4405-9ea8-2b4a6004c583</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='centres_services_main_modal_edit']/div/div/div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#centres_services_main_modal_edit > div.modal-dialog.modal-dialog-centered.modal-xl > div.modal-content > div.modal-header</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>aced909a-f05c-4db0-a8f7-986d25921e4b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-header</value>
      <webElementGuid>39f04686-7761-4b57-8c3d-00858fec5193</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    Edit Centres &amp; Services (Main)
                    
                    
                        
                            Bahasa IndonesiaChineseEnglishVietnamese
                            
                        
                    
                    
                        ×
                    
                </value>
      <webElementGuid>6f456ec3-4f99-4f85-9922-b54228a64500</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;centres_services_main_modal_edit&quot;)/div[@class=&quot;modal-dialog modal-dialog-centered modal-xl&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-header&quot;]</value>
      <webElementGuid>db57933e-d430-4cd5-8b60-ba342020047b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='centres_services_main_modal_edit']/div/div/div[2]</value>
      <webElementGuid>493ee695-5cd2-4b97-a01b-4b619f57c5fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading...'])[2]/following::div[1]</value>
      <webElementGuid>782d1245-1a3c-430a-9e08-233bb3fac611</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/following::div[6]</value>
      <webElementGuid>5ffb905c-8fce-44bb-9afb-423f8228b977</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Basic Information'])[2]/preceding::div[3]</value>
      <webElementGuid>af3c1f0e-4112-4bcb-b502-1ca12f711f62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div[2]</value>
      <webElementGuid>94a9e4a2-27d8-4c50-ac02-b0511f6c31a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    Edit Centres &amp; Services (Main)
                    
                    
                        
                            Bahasa IndonesiaChineseEnglishVietnamese
                            
                        
                    
                    
                        ×
                    
                ' or . = '
                    Edit Centres &amp; Services (Main)
                    
                    
                        
                            Bahasa IndonesiaChineseEnglishVietnamese
                            
                        
                    
                    
                        ×
                    
                ')]</value>
      <webElementGuid>a4953852-2b05-489f-b836-082bff9404e9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
