<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Send Centres  Services (Main)(s) For Approval</name>
   <tag></tag>
   <elementGuidId>aa1d8130-a766-482e-9730-f3be118815cf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='centres_services_main_modal_approval-send-approval']/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#centres_services_main_modal_approval-send-approval > div.modal-dialog.modal-dialog-centered > div.modal-content > div.modal-header</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f2dd9bbd-6c20-4b88-88c2-753f59d4e385</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-header</value>
      <webElementGuid>91b0ed54-fa12-4075-8c34-bddaa6c82cce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            Send Centres &amp; Services (Main)(s) For Approval
                            
                                ×
                            
                        </value>
      <webElementGuid>fea44d59-dc16-4a46-9ab2-24358bbad39a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;centres_services_main_modal_approval-send-approval&quot;)/div[@class=&quot;modal-dialog modal-dialog-centered&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-header&quot;]</value>
      <webElementGuid>7791c890-1fca-40e4-a902-4e7176dfd597</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='centres_services_main_modal_approval-send-approval']/div/div/div</value>
      <webElementGuid>dada7016-d514-4590-a711-4c23602bc051</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[2]/following::div[4]</value>
      <webElementGuid>0fe92f7e-68c8-4e12-b76b-60ae0bc2b4a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delete'])[1]/following::div[4]</value>
      <webElementGuid>1598b646-9145-4bf7-ab7e-d21dc388ac5d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div/div/div</value>
      <webElementGuid>75fe7fcc-35f4-44c8-babd-890cb551a252</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                            Send Centres &amp; Services (Main)(s) For Approval
                            
                                ×
                            
                        ' or . = '
                            Send Centres &amp; Services (Main)(s) For Approval
                            
                                ×
                            
                        ')]</value>
      <webElementGuid>d3a89906-a546-47f2-9f0e-05998ba69d82</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
