<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_LoginPassword</name>
   <tag></tag>
   <elementGuidId>423aef2f-2f9b-4b16-bb77-063254c0aed9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.row.d-flex.justify-content-center</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>7adc8e2b-361b-4500-90b1-bcd03b18b191</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row d-flex justify-content-center</value>
      <webElementGuid>cc64c1b1-9096-487f-b496-e2b1099a5cea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
						



								
									Username
								
								
								
									Password
								
								    Login
									Forgot Password?
									

						
					</value>
      <webElementGuid>f95195a0-f65e-4ac5-97c4-acd61f544c4b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;body&quot;]/div[@class=&quot;container text-center&quot;]/div[@class=&quot;parkway-body&quot;]/div[@class=&quot;row d-flex justify-content-center&quot;]</value>
      <webElementGuid>f8983d9b-f542-433e-af12-3c694d881f65</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]</value>
      <webElementGuid>5995be94-416a-419d-bada-e05e2f1e77b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
						



								
									Username
								
								
								
									Password
								
								    Login
									Forgot Password?
									

						
					' or . = '
						



								
									Username
								
								
								
									Password
								
								    Login
									Forgot Password?
									

						
					')]</value>
      <webElementGuid>7292d208-298e-4ca0-89df-1825cd9cb292</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
