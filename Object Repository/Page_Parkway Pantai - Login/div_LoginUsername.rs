<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_LoginUsername</name>
   <tag></tag>
   <elementGuidId>1da4c2d5-6845-4a5f-bcfa-40d201654df8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div/div[2]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.parkway-body</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>dfde6799-b1fd-4d19-9b4e-559c7f5e7b0f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>parkway-body</value>
      <webElementGuid>b3838592-20da-4460-9f95-d1116f626f5e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
						
                                                	
						
                                        
					
					
					
						



								
									Username
								
								
								
									Password
								
								    Login
									Forgot Password?
									

						
					
					
						Login with 
					
					
						
						
					
				</value>
      <webElementGuid>b1864558-8cf4-4561-80da-41d9f17636c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;body&quot;]/div[@class=&quot;container text-center&quot;]/div[@class=&quot;parkway-body&quot;]</value>
      <webElementGuid>88f4ec0e-42d9-4bf3-8e4f-9d109ae414d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>dffd4d31-c0fc-48a2-802e-65f979147c10</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>parkway-body</value>
      <webElementGuid>ec6984ec-1afb-4de4-84b2-93e03fc594b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
						
                                                	
						
                                        
					
					
					
						



								
									Username
								
								
								
									Password
								
								    Login
									Forgot Password?
									

						
					
					
						Login with 
					
					
						
						
					
				</value>
      <webElementGuid>044bc901-6b79-44ca-99c1-63e9ed697d9b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;body&quot;]/div[@class=&quot;container text-center&quot;]/div[@class=&quot;parkway-body&quot;]</value>
      <webElementGuid>f3fef0c0-8085-4d26-b14b-828fb8d54365</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a05dafc6-3064-4404-90d7-6b31092612f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>parkway-body</value>
      <webElementGuid>950adeeb-9558-4034-8c8b-931ff4b1ed93</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
						
                                                	
						
                                        
					
					
					
						



								
									Username
								
								
								
									Password
								
								    Login
									Forgot Password?
									

						
					
					
						Login with 
					
					
						
						
					
				</value>
      <webElementGuid>0c87e665-95c9-47c4-9090-8ee768928ce0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;body&quot;]/div[@class=&quot;container text-center&quot;]/div[@class=&quot;parkway-body&quot;]</value>
      <webElementGuid>b37af2be-a733-4fc2-92e6-cf3613532169</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>05fe819c-edc1-4a2e-8431-18c77531626e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>parkway-body</value>
      <webElementGuid>f4b3baf5-0e89-4231-b7e1-c1ef1b2031d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
						
                                                	
						
                                        
					
					
					
						



								
									Username
								
								
								
									Password
								
								    Login
									Forgot Password?
									

						
					
					
						Login with 
					
					
						
						
					
				</value>
      <webElementGuid>32a5b85a-4674-4bb3-bafb-5919cfb2664b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;body&quot;]/div[@class=&quot;container text-center&quot;]/div[@class=&quot;parkway-body&quot;]</value>
      <webElementGuid>d8e384a4-9cd3-4307-aa02-b9546968af90</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>44c3c7fd-d408-4c68-9f1a-e3dc2418754f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>parkway-body</value>
      <webElementGuid>82c9b98e-57c1-49be-af04-4989e5b86066</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
						
                                                	
						
                                        
					
					
					
						



								
									Username
								
								
								
									Password
								
								    Login
									Forgot Password?
									

						
					
					
						Login with 
					
					
						
						
					
				</value>
      <webElementGuid>3507eead-3517-4396-b939-1b213e0ad523</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;body&quot;]/div[@class=&quot;container text-center&quot;]/div[@class=&quot;parkway-body&quot;]</value>
      <webElementGuid>662a9748-8f4b-41b4-bff4-56a0694a6db2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div</value>
      <webElementGuid>4a7f737e-3b52-415d-9c3d-9ef02e4c5f67</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                        
						
                                                	
						
                                        
					
					
					
						



								
									Username
								
								
								
									Password
								
								    Login
									Forgot Password?
									

						
					
					
						Login with 
					
					
						
						
					
				' or . = '
                                        
						
                                                	
						
                                        
					
					
					
						



								
									Username
								
								
								
									Password
								
								    Login
									Forgot Password?
									

						
					
					
						Login with 
					
					
						
						
					
				')]</value>
      <webElementGuid>ddb039ca-dc3f-48b4-8aaf-a5c1c1e12daa</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
