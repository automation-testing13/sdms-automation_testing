<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Structured Data                        _67d0e0</name>
   <tag></tag>
   <elementGuidId>123d8680-d339-49cb-8af6-2765d836a231</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='collapseGroup']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.sidebar-menu-item.has-child.collapsed</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a38cac5c-2ed2-44f4-a8d5-ac6e83023ae4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>collapse</value>
      <webElementGuid>4a112f04-535f-498a-ab3f-8e4244e464aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-target</name>
      <type>Main</type>
      <value>#structured-dataMenu</value>
      <webElementGuid>140ae1ea-21ce-4ac6-b268-5faab8cfc9f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sidebar-menu-item has-child collapsed</value>
      <webElementGuid>b73eb1e8-bd37-4209-8668-5f7c47924f04</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>aea7e0ec-5e6b-4432-b6bb-fd1a714a6e2f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                            Structured Data
                            
                            
                                
                                    
                                        Centres &amp; Services (Main)
                                    
                                
                                
                                    
                                        Centres &amp; Services (Sub)
                                    
                                
                                
                                    
                                        Content Hub (Main)
                                    
                                
                                
                                    
                                        Content Hub (Sub)
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Events
                                    
                                
                                
                                    
                                        Highlights
                                    
                                
                                
                                    
                                        Locations
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                                
                                    
                                        Structured Campaign
                                    
                                
                                
                                    
                                        Structured Page
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                                
                                    
                                        Maternity Tours
                                    
                                
                            
                        </value>
      <webElementGuid>a8550fae-92c6-4110-95e6-a4a635667622</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;collapseGroup&quot;)/div[@class=&quot;sidebar-menu-item has-child collapsed&quot;]</value>
      <webElementGuid>05221ee0-30c8-4332-8c71-5f8499c276e6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='collapseGroup']/div</value>
      <webElementGuid>bb14d002-4d4c-49e3-84e5-8071e2d6e1ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/following::div[1]</value>
      <webElementGuid>46e143e1-c895-44e9-8246-679929bf6075</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[1]/following::div[2]</value>
      <webElementGuid>8aa8d2fb-a6d2-4895-9f9a-3f9984fb3db3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//nav/div/div</value>
      <webElementGuid>81209efb-e966-46ab-aba2-f27f0486fe27</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                            
                            Structured Data
                            
                            
                                
                                    
                                        Centres &amp; Services (Main)
                                    
                                
                                
                                    
                                        Centres &amp; Services (Sub)
                                    
                                
                                
                                    
                                        Content Hub (Main)
                                    
                                
                                
                                    
                                        Content Hub (Sub)
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Events
                                    
                                
                                
                                    
                                        Highlights
                                    
                                
                                
                                    
                                        Locations
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                                
                                    
                                        Structured Campaign
                                    
                                
                                
                                    
                                        Structured Page
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                                
                                    
                                        Maternity Tours
                                    
                                
                            
                        ' or . = '
                            
                            Structured Data
                            
                            
                                
                                    
                                        Centres &amp; Services (Main)
                                    
                                
                                
                                    
                                        Centres &amp; Services (Sub)
                                    
                                
                                
                                    
                                        Content Hub (Main)
                                    
                                
                                
                                    
                                        Content Hub (Sub)
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Events
                                    
                                
                                
                                    
                                        Highlights
                                    
                                
                                
                                    
                                        Locations
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                                
                                    
                                        Structured Campaign
                                    
                                
                                
                                    
                                        Structured Page
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                                
                                    
                                        Maternity Tours
                                    
                                
                            
                        ')]</value>
      <webElementGuid>ca7a362b-d7cd-4656-bc19-4f21274f8409</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
