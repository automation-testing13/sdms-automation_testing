<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Centres  Services (Main)</name>
   <tag></tag>
   <elementGuidId>3bbc501d-b19e-414d-8217-93623a6d6d41</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='structured-dataMenu']/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.sidebar-menu-item.sdms-access-centres-services-main.side-child-item</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>f14eb60a-0ee8-4c44-ad04-5ff2d9656c9b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>centres-services-main</value>
      <webElementGuid>15b32ac2-19ef-4e20-8fbd-febecfce4adc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sidebar-menu-item sdms-access-centres-services-main side-child-item</value>
      <webElementGuid>ecd20d40-bfba-4664-b2f6-1360e93f24c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        Centres &amp; Services (Main)
                                    </value>
      <webElementGuid>5b174afc-b693-4671-8806-486dd4d471cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;structured-dataMenu&quot;)/a[@class=&quot;sidebar-menu-item sdms-access-centres-services-main side-child-item&quot;]</value>
      <webElementGuid>20e9baa4-3153-4f65-bad1-d8c8605e0257</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='structured-dataMenu']/a</value>
      <webElementGuid>acf376f5-ec82-40b4-93a9-d04809b51739</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Structured Data'])[1]/following::a[1]</value>
      <webElementGuid>6cf3fc3b-fe16-4e65-8570-e5d314220fd7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/following::a[1]</value>
      <webElementGuid>0322fc77-5189-45fe-a4b6-d3f05a9b2286</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Centres &amp; Services (Sub)'])[1]/preceding::a[1]</value>
      <webElementGuid>67e8b24f-9aca-4a38-9dcc-206e77b2da82</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'centres-services-main')]</value>
      <webElementGuid>34b845fb-60aa-448a-a108-595e6a73891b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//nav/div/div/div/a</value>
      <webElementGuid>79c43c8e-1b9c-4667-9910-3c7bd3a19ca5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'centres-services-main' and (text() = '
                                        Centres &amp; Services (Main)
                                    ' or . = '
                                        Centres &amp; Services (Main)
                                    ')]</value>
      <webElementGuid>1fcf9a03-e400-4c52-ad31-186de1867df3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
