<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Structured Data</name>
   <tag></tag>
   <elementGuidId>40755f39-b91f-470a-ab32-c5c8f752bfff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='collapseGroup']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.sidebar-menu-item.has-child.collapsed</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a06c0dfb-2282-4dca-8e76-aec792fc05bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>collapse</value>
      <webElementGuid>7a548970-963b-435a-957b-491bea182a06</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-target</name>
      <type>Main</type>
      <value>#structured-dataMenu</value>
      <webElementGuid>d0c6a722-b80a-4095-9a8c-e4c2a8847b8c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sidebar-menu-item has-child collapsed</value>
      <webElementGuid>04aee3d7-0fb5-4a18-ba32-286a7dcc3b17</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>042d3e4e-6602-46e0-ab85-17ec8bb19bff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                            Structured Data
                            
                            
                                
                                    
                                        Centres &amp; Services (Main)
                                    
                                
                                
                                    
                                        Centres &amp; Services (Sub)
                                    
                                
                                
                                    
                                        Content Hub (Main)
                                    
                                
                                
                                    
                                        Content Hub (Sub)
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Events
                                    
                                
                                
                                    
                                        Highlights
                                    
                                
                                
                                    
                                        Locations
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                                
                                    
                                        Structured Campaign
                                    
                                
                                
                                    
                                        Structured Page
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                                
                                    
                                        Maternity Tours
                                    
                                
                            
                        </value>
      <webElementGuid>d8fd5c77-c420-4b66-bbf3-4a1b329fc000</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;collapseGroup&quot;)/div[@class=&quot;sidebar-menu-item has-child collapsed&quot;]</value>
      <webElementGuid>7719699a-a5fd-47ae-9f53-233af7de969d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>610c52ac-2a08-4968-b19e-f266b8a78771</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>collapse</value>
      <webElementGuid>d73553d6-ad4c-4d9b-8e02-ee5d6a78bc30</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-target</name>
      <type>Main</type>
      <value>#structured-dataMenu</value>
      <webElementGuid>219dfef3-c8bc-409d-a7d2-dcc6774bc627</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sidebar-menu-item has-child collapsed</value>
      <webElementGuid>62c7d4e0-8f62-4721-aea8-ce3874d4688e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>711e2cfb-7042-4caf-8490-12516e0613b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                            Structured Data
                            
                            
                                
                                    
                                        Centres &amp; Services (Main)
                                    
                                
                                
                                    
                                        Centres &amp; Services (Sub)
                                    
                                
                                
                                    
                                        Content Hub (Main)
                                    
                                
                                
                                    
                                        Content Hub (Sub)
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Events
                                    
                                
                                
                                    
                                        Highlights
                                    
                                
                                
                                    
                                        Locations
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                                
                                    
                                        Structured Campaign
                                    
                                
                                
                                    
                                        Structured Page
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                                
                                    
                                        Maternity Tours
                                    
                                
                            
                        </value>
      <webElementGuid>43d6cb65-31c4-4247-b71f-02dcd35df9fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;collapseGroup&quot;)/div[@class=&quot;sidebar-menu-item has-child collapsed&quot;]</value>
      <webElementGuid>64aaa491-1853-4968-ba4d-c3c5b03a80fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b4d6c56c-7574-44e9-9686-6d415dbcb02a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>collapse</value>
      <webElementGuid>4f55a35a-ae31-4e14-b1be-17ebeee07aae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-target</name>
      <type>Main</type>
      <value>#structured-dataMenu</value>
      <webElementGuid>7ae0c678-9406-4d55-9c65-52ac8c09aace</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sidebar-menu-item has-child collapsed</value>
      <webElementGuid>32f18296-2027-4d23-a439-7d9a4961fa87</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>46bcbe41-77d9-4bf8-91be-2192df269b4b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                            Structured Data
                            
                            
                                
                                    
                                        Centres &amp; Services (Main)
                                    
                                
                                
                                    
                                        Centres &amp; Services (Sub)
                                    
                                
                                
                                    
                                        Content Hub (Main)
                                    
                                
                                
                                    
                                        Content Hub (Sub)
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Events
                                    
                                
                                
                                    
                                        Highlights
                                    
                                
                                
                                    
                                        Locations
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                                
                                    
                                        Structured Campaign
                                    
                                
                                
                                    
                                        Structured Page
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                                
                                    
                                        Maternity Tours
                                    
                                
                            
                        </value>
      <webElementGuid>70d74a48-a464-46b2-9514-71cbf3626ca3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;collapseGroup&quot;)/div[@class=&quot;sidebar-menu-item has-child collapsed&quot;]</value>
      <webElementGuid>ab9192f2-702c-485f-a9ab-dea6fe88886a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>72d46a15-9890-4d83-ae8c-1ea928e89ffc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>collapse</value>
      <webElementGuid>653d0755-88e0-42ec-b6a8-73ce64f2aaa7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-target</name>
      <type>Main</type>
      <value>#structured-dataMenu</value>
      <webElementGuid>8e7c7794-490c-4010-bea0-2809b75bf083</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sidebar-menu-item has-child collapsed</value>
      <webElementGuid>2ff80d37-5cac-46e0-aaff-3f463dcdcfbe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>07c44aa2-e823-4f5b-b43c-c6af43549a7d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                            Structured Data
                            
                            
                                
                                    
                                        Centres &amp; Services (Main)
                                    
                                
                                
                                    
                                        Centres &amp; Services (Sub)
                                    
                                
                                
                                    
                                        Content Hub (Main)
                                    
                                
                                
                                    
                                        Content Hub (Sub)
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Events
                                    
                                
                                
                                    
                                        Highlights
                                    
                                
                                
                                    
                                        Locations
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                                
                                    
                                        Structured Campaign
                                    
                                
                                
                                    
                                        Structured Page
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                                
                                    
                                        Maternity Tours
                                    
                                
                            
                        </value>
      <webElementGuid>f4497491-a479-4a35-a8a2-aa03954d18db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;collapseGroup&quot;)/div[@class=&quot;sidebar-menu-item has-child collapsed&quot;]</value>
      <webElementGuid>6f1f6d0b-8dce-4ee4-b5b8-4b1888d09a03</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='collapseGroup']/div</value>
      <webElementGuid>0dab106e-916d-4cbd-a549-83801961ab11</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home'])[2]/following::div[1]</value>
      <webElementGuid>534986ef-6135-41bd-92e9-177be705be9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[1]/following::div[2]</value>
      <webElementGuid>b2e20e03-e9b3-452c-9a00-259a70a6b389</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//nav/div/div</value>
      <webElementGuid>c11e9739-4e7d-4855-b3f4-5ed85ce75950</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                            
                            Structured Data
                            
                            
                                
                                    
                                        Centres &amp; Services (Main)
                                    
                                
                                
                                    
                                        Centres &amp; Services (Sub)
                                    
                                
                                
                                    
                                        Content Hub (Main)
                                    
                                
                                
                                    
                                        Content Hub (Sub)
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Events
                                    
                                
                                
                                    
                                        Highlights
                                    
                                
                                
                                    
                                        Locations
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                                
                                    
                                        Structured Campaign
                                    
                                
                                
                                    
                                        Structured Page
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                                
                                    
                                        Maternity Tours
                                    
                                
                            
                        ' or . = '
                            
                            Structured Data
                            
                            
                                
                                    
                                        Centres &amp; Services (Main)
                                    
                                
                                
                                    
                                        Centres &amp; Services (Sub)
                                    
                                
                                
                                    
                                        Content Hub (Main)
                                    
                                
                                
                                    
                                        Content Hub (Sub)
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Events
                                    
                                
                                
                                    
                                        Highlights
                                    
                                
                                
                                    
                                        Locations
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                                
                                    
                                        Structured Campaign
                                    
                                
                                
                                    
                                        Structured Page
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                                
                                    
                                        Maternity Tours
                                    
                                
                            
                        ')]</value>
      <webElementGuid>374023fd-8232-4dfe-83f1-0caec148a724</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
