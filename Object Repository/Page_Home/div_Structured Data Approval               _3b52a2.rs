<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Structured Data Approval               _3b52a2</name>
   <tag></tag>
   <elementGuidId>26a505d6-c10e-45a3-8a3d-c5c31d256ee3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='collapseGroup']/div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>ec624337-aa70-47dc-8bef-85a5138ef1af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>collapse</value>
      <webElementGuid>d2dc50a0-346b-46f1-adad-3cfde6171539</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-target</name>
      <type>Main</type>
      <value>#structured-data-approvalsMenu</value>
      <webElementGuid>a0d31db0-34df-4270-a7f4-048b0d6184c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sidebar-menu-item has-child collapsed</value>
      <webElementGuid>780e7b97-dcbf-421b-b45c-93a66972edb6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>938cc8b2-283d-4c7e-a2d1-d6195acbcd18</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                            Structured Data Approval
                            
                            
                                
                                    
                                        Centres &amp; Services (Main)
                                    
                                
                                
                                    
                                        Centres &amp; Services (Sub)
                                    
                                
                                
                                    
                                        Content Hub (Main)
                                    
                                
                                
                                    
                                        Content Hub (Sub)
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Events
                                    
                                
                                
                                    
                                        Highlights
                                    
                                
                                
                                    
                                        Locations
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                                
                                    
                                        Structured Campaign
                                    
                                
                                
                                    
                                        Structured Page
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                                
                                    
                                        Maternity Tours
                                    
                                
                            
                        </value>
      <webElementGuid>0ec1b1ed-8718-4eb4-a1e2-c8149ed420ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;collapseGroup&quot;)/div[@class=&quot;sidebar-menu-item has-child collapsed&quot;]</value>
      <webElementGuid>fdcf71a6-135f-4ef4-9349-5bfd4ba7957b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='collapseGroup']/div[3]</value>
      <webElementGuid>77d6fc81-7477-468a-b4ca-6c2e2bde8204</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Event Type'])[1]/following::div[1]</value>
      <webElementGuid>fb57029f-7702-4e64-b004-ca479a684fc9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Audience'])[1]/following::div[1]</value>
      <webElementGuid>3b277745-7152-4f95-95ba-53dcf3e91df8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]</value>
      <webElementGuid>13ef9a1f-52d3-40b0-85aa-f2cea7e79b13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                            
                            Structured Data Approval
                            
                            
                                
                                    
                                        Centres &amp; Services (Main)
                                    
                                
                                
                                    
                                        Centres &amp; Services (Sub)
                                    
                                
                                
                                    
                                        Content Hub (Main)
                                    
                                
                                
                                    
                                        Content Hub (Sub)
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Events
                                    
                                
                                
                                    
                                        Highlights
                                    
                                
                                
                                    
                                        Locations
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                                
                                    
                                        Structured Campaign
                                    
                                
                                
                                    
                                        Structured Page
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                                
                                    
                                        Maternity Tours
                                    
                                
                            
                        ' or . = '
                            
                            Structured Data Approval
                            
                            
                                
                                    
                                        Centres &amp; Services (Main)
                                    
                                
                                
                                    
                                        Centres &amp; Services (Sub)
                                    
                                
                                
                                    
                                        Content Hub (Main)
                                    
                                
                                
                                    
                                        Content Hub (Sub)
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Events
                                    
                                
                                
                                    
                                        Highlights
                                    
                                
                                
                                    
                                        Locations
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                                
                                    
                                        Structured Campaign
                                    
                                
                                
                                    
                                        Structured Page
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                                
                                    
                                        Maternity Tours
                                    
                                
                            
                        ')]</value>
      <webElementGuid>4cb6ab3a-2a6f-4e5b-a344-fcbc77554765</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
