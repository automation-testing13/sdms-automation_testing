<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Structured Data                        _67d0e0</name>
   <tag></tag>
   <elementGuidId>681051ac-4d61-482d-b208-33c3c1c5fc6b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='collapseGroup']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.sidebar-menu-item.has-child.collapsed</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>725dfeb1-555e-45c6-85f3-5f820896a6b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>collapse</value>
      <webElementGuid>6a5256f0-63f9-483a-9973-4006b118a3de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-target</name>
      <type>Main</type>
      <value>#structured-dataMenu</value>
      <webElementGuid>5a78b6bb-f224-4de4-b528-c015cb96c4aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sidebar-menu-item has-child collapsed</value>
      <webElementGuid>48d432a7-265e-44e7-ab01-c456035d330a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>785cdbf2-5cad-4307-a24d-f396ce9f64e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                            Structured Data
                            
                            
                                
                                    
                                        Centres &amp; Services (Main)
                                    
                                
                                
                                    
                                        Centres &amp; Services (Sub)
                                    
                                
                                
                                    
                                        Content Hub (Main)
                                    
                                
                                
                                    
                                        Content Hub (Sub)
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Events
                                    
                                
                                
                                    
                                        Highlights
                                    
                                
                                
                                    
                                        Locations
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                                
                                    
                                        Structured Campaign
                                    
                                
                                
                                    
                                        Structured Page
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                                
                                    
                                        Maternity Tours
                                    
                                
                            
                        </value>
      <webElementGuid>80fa840f-6b0d-4f86-98dd-9faf89596c79</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;collapseGroup&quot;)/div[@class=&quot;sidebar-menu-item has-child collapsed&quot;]</value>
      <webElementGuid>1bdb381d-2634-4204-a069-31df31fd8bb1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b51cd06b-9f91-4d4e-9163-e5e5ac53536d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>collapse</value>
      <webElementGuid>36ebe9f3-80e9-423a-9a30-7fd2baae3059</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-target</name>
      <type>Main</type>
      <value>#structured-dataMenu</value>
      <webElementGuid>d6aafb4c-834f-4f12-8b1d-3ee2194900db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sidebar-menu-item has-child collapsed</value>
      <webElementGuid>aa1c15a8-afef-43ee-a9a7-7b3954304c1a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>645d5850-354f-458d-9e6f-519769b865bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                            Structured Data
                            
                            
                                
                                    
                                        Centres &amp; Services (Main)
                                    
                                
                                
                                    
                                        Centres &amp; Services (Sub)
                                    
                                
                                
                                    
                                        Content Hub (Main)
                                    
                                
                                
                                    
                                        Content Hub (Sub)
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Events
                                    
                                
                                
                                    
                                        Highlights
                                    
                                
                                
                                    
                                        Locations
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                                
                                    
                                        Structured Campaign
                                    
                                
                                
                                    
                                        Structured Page
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                                
                                    
                                        Maternity Tours
                                    
                                
                            
                        </value>
      <webElementGuid>4ffedb5b-19e5-4bce-94d1-1f280466b4f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;collapseGroup&quot;)/div[@class=&quot;sidebar-menu-item has-child collapsed&quot;]</value>
      <webElementGuid>83e1b9ab-186a-4a3c-9c4b-16a30dbe08bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>35421cb6-a3c7-4547-90cb-72673da8c953</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>collapse</value>
      <webElementGuid>ce534e4e-c410-410e-ad72-bccb761e2ab2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-target</name>
      <type>Main</type>
      <value>#structured-dataMenu</value>
      <webElementGuid>bdd8d2e8-f56c-455e-a17e-97c7e562c1af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sidebar-menu-item has-child collapsed</value>
      <webElementGuid>18d07a9b-9df1-4467-b6cf-c5f0c44cc94e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>6493328d-5d70-47e7-b14d-4d38d76fc921</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                            Structured Data
                            
                            
                                
                                    
                                        Centres &amp; Services (Main)
                                    
                                
                                
                                    
                                        Centres &amp; Services (Sub)
                                    
                                
                                
                                    
                                        Content Hub (Main)
                                    
                                
                                
                                    
                                        Content Hub (Sub)
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Events
                                    
                                
                                
                                    
                                        Highlights
                                    
                                
                                
                                    
                                        Locations
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                                
                                    
                                        Structured Campaign
                                    
                                
                                
                                    
                                        Structured Page
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                                
                                    
                                        Maternity Tours
                                    
                                
                            
                        </value>
      <webElementGuid>1b978ecd-e836-485e-882a-151fe3dbf19b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;collapseGroup&quot;)/div[@class=&quot;sidebar-menu-item has-child collapsed&quot;]</value>
      <webElementGuid>0fb28450-0eec-4068-8d8c-89af3abb1281</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b2f47b7e-7661-4a5b-986f-b4ecbd4a0a4b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>collapse</value>
      <webElementGuid>cee3f572-18a9-4d5f-93bb-1e884e9551eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-target</name>
      <type>Main</type>
      <value>#structured-dataMenu</value>
      <webElementGuid>a031dffb-9d05-4ba7-9078-27f13bdc83e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sidebar-menu-item has-child collapsed</value>
      <webElementGuid>cc0e652b-19bc-4a00-8159-4e21c47f6f21</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>22f5116b-3a67-4db0-a8ec-05fca159f4a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                            Structured Data
                            
                            
                                
                                    
                                        Centres &amp; Services (Main)
                                    
                                
                                
                                    
                                        Centres &amp; Services (Sub)
                                    
                                
                                
                                    
                                        Content Hub (Main)
                                    
                                
                                
                                    
                                        Content Hub (Sub)
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Events
                                    
                                
                                
                                    
                                        Highlights
                                    
                                
                                
                                    
                                        Locations
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                                
                                    
                                        Structured Campaign
                                    
                                
                                
                                    
                                        Structured Page
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                                
                                    
                                        Maternity Tours
                                    
                                
                            
                        </value>
      <webElementGuid>742be075-2e2b-4b6c-ad94-029f4b6922aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;collapseGroup&quot;)/div[@class=&quot;sidebar-menu-item has-child collapsed&quot;]</value>
      <webElementGuid>2487abd5-190b-440e-abb6-ffc9b7d42646</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>77c80f70-a1fe-4b97-83de-de6a1ac991a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>collapse</value>
      <webElementGuid>a3ac29d8-71af-4a7c-8b5c-9bd0cd1b1f43</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-target</name>
      <type>Main</type>
      <value>#structured-dataMenu</value>
      <webElementGuid>bc6a4ebc-a540-4fbc-a969-305c79d8eb51</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sidebar-menu-item has-child collapsed</value>
      <webElementGuid>e83cc158-c98c-4279-8b95-69b625fabd3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>54780a98-8e4e-4299-b199-4ba4ea05651c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                            Structured Data
                            
                            
                                
                                    
                                        Centres &amp; Services (Main)
                                    
                                
                                
                                    
                                        Centres &amp; Services (Sub)
                                    
                                
                                
                                    
                                        Content Hub (Main)
                                    
                                
                                
                                    
                                        Content Hub (Sub)
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Events
                                    
                                
                                
                                    
                                        Highlights
                                    
                                
                                
                                    
                                        Locations
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                                
                                    
                                        Structured Campaign
                                    
                                
                                
                                    
                                        Structured Page
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                                
                                    
                                        Maternity Tours
                                    
                                
                            
                        </value>
      <webElementGuid>af103e75-a17e-4493-a4fc-113a194e996f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;collapseGroup&quot;)/div[@class=&quot;sidebar-menu-item has-child collapsed&quot;]</value>
      <webElementGuid>fd87aafd-3363-40a4-83f3-3d0670eb8bc5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c2836e73-8653-4411-8b17-a3f71e59316f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>collapse</value>
      <webElementGuid>174fc082-a890-40c3-9c21-d95e0d1bb3ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-target</name>
      <type>Main</type>
      <value>#structured-dataMenu</value>
      <webElementGuid>60903336-d554-4b22-8f58-65e21f70cb98</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sidebar-menu-item has-child collapsed</value>
      <webElementGuid>4dbb90c2-7518-49ce-ae89-cfcec47fccd8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>81a8b38f-c440-4589-8d97-60875e197f2f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                            Structured Data
                            
                            
                                
                                    
                                        Centres &amp; Services (Main)
                                    
                                
                                
                                    
                                        Centres &amp; Services (Sub)
                                    
                                
                                
                                    
                                        Content Hub (Main)
                                    
                                
                                
                                    
                                        Content Hub (Sub)
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Events
                                    
                                
                                
                                    
                                        Highlights
                                    
                                
                                
                                    
                                        Locations
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                                
                                    
                                        Structured Campaign
                                    
                                
                                
                                    
                                        Structured Page
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                                
                                    
                                        Maternity Tours
                                    
                                
                            
                        </value>
      <webElementGuid>70b4780f-00c9-4a6f-84ba-faa12f0dc8de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;collapseGroup&quot;)/div[@class=&quot;sidebar-menu-item has-child collapsed&quot;]</value>
      <webElementGuid>c3cbe855-b2a6-4fb9-9a1c-c98c2e856c92</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='collapseGroup']/div</value>
      <webElementGuid>30245b5d-a7b4-428e-949b-2c27044ee6dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home'])[2]/following::div[1]</value>
      <webElementGuid>56f9b370-c474-4d38-a9f9-ce3a2072670a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[1]/following::div[2]</value>
      <webElementGuid>92437a08-d4f1-4ede-9f2b-dbf07e3387cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//nav/div/div</value>
      <webElementGuid>a0dee4e1-d0c7-4b32-a43a-2046259522fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                            
                            Structured Data
                            
                            
                                
                                    
                                        Centres &amp; Services (Main)
                                    
                                
                                
                                    
                                        Centres &amp; Services (Sub)
                                    
                                
                                
                                    
                                        Content Hub (Main)
                                    
                                
                                
                                    
                                        Content Hub (Sub)
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Events
                                    
                                
                                
                                    
                                        Highlights
                                    
                                
                                
                                    
                                        Locations
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                                
                                    
                                        Structured Campaign
                                    
                                
                                
                                    
                                        Structured Page
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                                
                                    
                                        Maternity Tours
                                    
                                
                            
                        ' or . = '
                            
                            Structured Data
                            
                            
                                
                                    
                                        Centres &amp; Services (Main)
                                    
                                
                                
                                    
                                        Centres &amp; Services (Sub)
                                    
                                
                                
                                    
                                        Content Hub (Main)
                                    
                                
                                
                                    
                                        Content Hub (Sub)
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Events
                                    
                                
                                
                                    
                                        Highlights
                                    
                                
                                
                                    
                                        Locations
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                                
                                    
                                        Structured Campaign
                                    
                                
                                
                                    
                                        Structured Page
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                                
                                    
                                        Maternity Tours
                                    
                                
                            
                        ')]</value>
      <webElementGuid>4733d95f-3c3f-4718-9a4d-14c5a0663f32</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
