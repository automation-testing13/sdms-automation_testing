<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>main_Home                                                                                    Structured Data</name>
   <tag></tag>
   <elementGuidId>d71b77ee-b0d0-4aca-8b6f-4d1a6714528c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//main[@id='index-page-app']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#index-page-app</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>main</value>
      <webElementGuid>264e54bd-7a08-4694-a428-519774354a97</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>index-page-app</value>
      <webElementGuid>2fb59cc4-b0eb-4762-940a-ad4dfd38cc5b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>page-contents p-0</value>
      <webElementGuid>7c29097c-0ee4-41df-bad8-4cbaa4842ec0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
            
                Home
            

            
                
                    
                        Structured Data
                        
                            
                                
                                    
                                        Clinic
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                            
                            
                        
                    
                    
                        Structured Data Approvals
                        
                            
                                
                                    
                                        Clinic
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                            
                            
                        
                    
                    
                        Controlled List
                        
                            
                                
                                    
                                        Associated Treatments
                                    
                                
                                
                                    
                                        Body Parts (Parent L1)
                                    
                                
                                
                                    
                                        Body Parts (Parent L2)
                                    
                                
                                
                                    
                                        Clinical Interest
                                    
                                
                                
                                    
                                        Conditions and Diseases
                                    
                                
                                
                                    
                                        Country
                                    
                                
                                
                                    
                                        Country of Residence
                                    
                                
                                
                                    
                                        Gender
                                    
                                
                                
                                    
                                        Hospitals
                                    
                                
                                
                                    
                                        Insurance Panel
                                    
                                
                                
                                    
                                        Languages
                                    
                                
                                
                                    
                                        Medical Professional Type
                                    
                                
                                
                                    
                                        Nationality
                                    
                                
                                
                                    
                                        Service Provider Type
                                    
                                
                                
                                    
                                        Specialty (Parent L1)
                                    
                                
                                
                                    
                                        Specialty (Parent L2)
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                            
                            
                        
                    
                    
                        Controlled List Approvals
                        
                            
                                
                                    
                                        Associated Treatments
                                    
                                
                                
                                    
                                        Body Parts (Parent L1)
                                    
                                
                                
                                    
                                        Body Parts (Parent L2)
                                    
                                
                                
                                    
                                        Clinical Interest
                                    
                                
                                
                                    
                                        Conditions and Diseases
                                    
                                
                                
                                    
                                        Country of Residence
                                    
                                
                                
                                    
                                        Country
                                    
                                
                                
                                    
                                        Gender
                                    
                                
                                
                                    
                                        Hospitals
                                    
                                
                                
                                    
                                        Insurance Panel
                                    
                                
                                
                                    
                                        Languages
                                    
                                
                                
                                    
                                        Medical Professional Type
                                    
                                
                                
                                    
                                        Nationality
                                    
                                
                                
                                    
                                        Service Provider Type
                                    
                                
                                
                                    
                                        Specialty (Parent L1)
                                    
                                
                                
                                    
                                        Specialty (Parent L2)
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                            
                            
                        
                    
                
            
        
    </value>
      <webElementGuid>0a4d90c4-1391-448d-83a5-6071edb9b372</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;index-page-app&quot;)</value>
      <webElementGuid>938951b4-74a0-47de-a06b-1d58c4c924be</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//main[@id='index-page-app']</value>
      <webElementGuid>746b0c3b-8ef7-4351-8a45-382f7fb59cb4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[2]/following::main[1]</value>
      <webElementGuid>3930867b-9896-41ae-96c1-d2caedbfacc5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test and Treatments'])[4]/following::main[1]</value>
      <webElementGuid>6c8d91c7-f187-4b32-ba3a-55d2c36223a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//main</value>
      <webElementGuid>67c285c0-7434-460a-bd1d-593df3843ba3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//main[@id = 'index-page-app' and (text() = '
        
            
                Home
            

            
                
                    
                        Structured Data
                        
                            
                                
                                    
                                        Clinic
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                            
                            
                        
                    
                    
                        Structured Data Approvals
                        
                            
                                
                                    
                                        Clinic
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                            
                            
                        
                    
                    
                        Controlled List
                        
                            
                                
                                    
                                        Associated Treatments
                                    
                                
                                
                                    
                                        Body Parts (Parent L1)
                                    
                                
                                
                                    
                                        Body Parts (Parent L2)
                                    
                                
                                
                                    
                                        Clinical Interest
                                    
                                
                                
                                    
                                        Conditions and Diseases
                                    
                                
                                
                                    
                                        Country
                                    
                                
                                
                                    
                                        Country of Residence
                                    
                                
                                
                                    
                                        Gender
                                    
                                
                                
                                    
                                        Hospitals
                                    
                                
                                
                                    
                                        Insurance Panel
                                    
                                
                                
                                    
                                        Languages
                                    
                                
                                
                                    
                                        Medical Professional Type
                                    
                                
                                
                                    
                                        Nationality
                                    
                                
                                
                                    
                                        Service Provider Type
                                    
                                
                                
                                    
                                        Specialty (Parent L1)
                                    
                                
                                
                                    
                                        Specialty (Parent L2)
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                            
                            
                        
                    
                    
                        Controlled List Approvals
                        
                            
                                
                                    
                                        Associated Treatments
                                    
                                
                                
                                    
                                        Body Parts (Parent L1)
                                    
                                
                                
                                    
                                        Body Parts (Parent L2)
                                    
                                
                                
                                    
                                        Clinical Interest
                                    
                                
                                
                                    
                                        Conditions and Diseases
                                    
                                
                                
                                    
                                        Country of Residence
                                    
                                
                                
                                    
                                        Country
                                    
                                
                                
                                    
                                        Gender
                                    
                                
                                
                                    
                                        Hospitals
                                    
                                
                                
                                    
                                        Insurance Panel
                                    
                                
                                
                                    
                                        Languages
                                    
                                
                                
                                    
                                        Medical Professional Type
                                    
                                
                                
                                    
                                        Nationality
                                    
                                
                                
                                    
                                        Service Provider Type
                                    
                                
                                
                                    
                                        Specialty (Parent L1)
                                    
                                
                                
                                    
                                        Specialty (Parent L2)
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                            
                            
                        
                    
                
            
        
    ' or . = '
        
            
                Home
            

            
                
                    
                        Structured Data
                        
                            
                                
                                    
                                        Clinic
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                            
                            
                        
                    
                    
                        Structured Data Approvals
                        
                            
                                
                                    
                                        Clinic
                                    
                                
                                
                                    
                                        Condition &amp; Disease
                                    
                                
                                
                                    
                                        Media Coverage
                                    
                                
                                
                                    
                                        Medical Professional
                                    
                                
                                
                                    
                                        Patient Assistance Centre
                                    
                                
                                
                                    
                                        Service Provider
                                    
                                
                                
                                    
                                        Specialization
                                    
                                
                                
                                    
                                        Specialty
                                    
                                
                            
                            
                        
                    
                    
                        Controlled List
                        
                            
                                
                                    
                                        Associated Treatments
                                    
                                
                                
                                    
                                        Body Parts (Parent L1)
                                    
                                
                                
                                    
                                        Body Parts (Parent L2)
                                    
                                
                                
                                    
                                        Clinical Interest
                                    
                                
                                
                                    
                                        Conditions and Diseases
                                    
                                
                                
                                    
                                        Country
                                    
                                
                                
                                    
                                        Country of Residence
                                    
                                
                                
                                    
                                        Gender
                                    
                                
                                
                                    
                                        Hospitals
                                    
                                
                                
                                    
                                        Insurance Panel
                                    
                                
                                
                                    
                                        Languages
                                    
                                
                                
                                    
                                        Medical Professional Type
                                    
                                
                                
                                    
                                        Nationality
                                    
                                
                                
                                    
                                        Service Provider Type
                                    
                                
                                
                                    
                                        Specialty (Parent L1)
                                    
                                
                                
                                    
                                        Specialty (Parent L2)
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                            
                            
                        
                    
                    
                        Controlled List Approvals
                        
                            
                                
                                    
                                        Associated Treatments
                                    
                                
                                
                                    
                                        Body Parts (Parent L1)
                                    
                                
                                
                                    
                                        Body Parts (Parent L2)
                                    
                                
                                
                                    
                                        Clinical Interest
                                    
                                
                                
                                    
                                        Conditions and Diseases
                                    
                                
                                
                                    
                                        Country of Residence
                                    
                                
                                
                                    
                                        Country
                                    
                                
                                
                                    
                                        Gender
                                    
                                
                                
                                    
                                        Hospitals
                                    
                                
                                
                                    
                                        Insurance Panel
                                    
                                
                                
                                    
                                        Languages
                                    
                                
                                
                                    
                                        Medical Professional Type
                                    
                                
                                
                                    
                                        Nationality
                                    
                                
                                
                                    
                                        Service Provider Type
                                    
                                
                                
                                    
                                        Specialty (Parent L1)
                                    
                                
                                
                                    
                                        Specialty (Parent L2)
                                    
                                
                                
                                    
                                        Test and Treatments
                                    
                                
                            
                            
                        
                    
                
            
        
    ')]</value>
      <webElementGuid>295033bf-4789-4cf1-8056-8c0527d879e2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
