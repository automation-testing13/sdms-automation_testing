1
00:00:00,647 --> 00:00:00,731
3. Story: Get list of Centres & Services (Main)

2
00:00:00,742 --> 00:00:01,221
9. User must logged in.
To login refer to TC1.0 - User Authentication.

3
00:00:01,222 --> 00:00:05,818
3. Enter url "https://uat.azlabs.sg/sdms"

4
00:00:05,818 --> 00:00:05,998
9. Redirecting to SSO Login page

5
00:00:05,999 --> 00:00:07,759
15. Verify SSO Login page reached

6
00:00:07,779 --> 00:00:09,062
21. Enter login username

7
00:00:09,062 --> 00:00:09,968
27. Enter login password

8
00:00:09,969 --> 00:00:12,409
33. Click login button

9
00:00:12,409 --> 00:00:13,916
15. SDMS landing page

10
00:00:13,917 --> 00:00:15,096
21. Structured Data button menu

11
00:00:15,097 --> 00:00:15,464
27. Click " Structured Data" from left navigation bar

12
00:00:15,465 --> 00:00:16,686
33. Centres & Services (Main) button menu

13
00:00:16,686 --> 00:00:18,814
39. Click  "Centres & Services (Main)" from child navigation bar

14
00:00:18,815 --> 00:00:28,653
43. waitForElementPresent(findTestObject("Object Repository/Page_Centres  Services (Main)/header_Centres  Services (Main)"), 1)

15
00:00:28,655 --> 00:00:30,503
49. List of "Centres & Services (Main)" list is shown in page

16
00:00:30,504 --> 00:00:30,696
53. closeBrowser()

