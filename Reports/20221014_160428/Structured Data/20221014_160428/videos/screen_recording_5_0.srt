1
00:00:00,226 --> 00:00:00,289
3. User must logged in.
To loginn refer to TC1.0 - User Authentication

2
00:00:00,289 --> 00:00:02,933
3. Enter url "https://uat.azlabs.sg/sdms"

3
00:00:02,933 --> 00:00:03,065
9. Redirecting to SSO Login page

4
00:00:03,066 --> 00:00:04,564
15. Verify SSO Login page reached

5
00:00:04,565 --> 00:00:05,472
21. Enter login username

6
00:00:05,473 --> 00:00:06,215
27. Enter login password

7
00:00:06,215 --> 00:00:08,567
33. Click login button

8
00:00:08,568 --> 00:00:09,829
9. SDMS landing page

9
00:00:09,829 --> 00:00:14,335
13. takeElementScreenshot(findTestObject("Object Repository/Page_Home old/div_Structured Data                        _67d0e0"))

10
00:00:14,336 --> 00:00:18,854
19. Click " Structured Data" from left navigation bar

11
00:00:18,855 --> 00:00:20,641
23. takeElementScreenshot(findTestObject("Object Repository/Page_Home old/span_Centres  Services (Main)"))

12
00:00:20,642 --> 00:00:26,674
29. Click  "Centres & Services (Main)" from child navigation bar

13
00:00:26,674 --> 00:00:30,707
35. List of "Centres & Services (Main)" list is shown in page

14
00:00:30,708 --> 00:00:31,683
39. setText(findTestObject("Object Repository/Page_Centres  Services (Main)/input_Search_centres_services_main-input-search"), "katalon")

15
00:00:31,683 --> 00:00:32,065
43. click(findTestObject("Object Repository/Page_Centres  Services (Main)/button_Search"))

16
00:00:32,065 --> 00:00:32,880
47. setText(findTestObject("Object Repository/Page_Centres  Services (Main)/input_Search_centres_services_main-input-search"), "katalon")

17
00:00:32,880 --> 00:00:33,281
51. click(findTestObject("Object Repository/Page_Centres  Services (Main)/button_Draft_centres_services_main-button-edit"))

18
00:00:33,281 --> 00:00:36,157
55. waitForElementPresent(findTestObject("Object Repository/Page_Centres  Services (Main)/div_Edit Centres  Services (Main)          _24fcef"), 1)

19
00:00:36,157 --> 00:00:36,523
59. click(findTestObject("Object Repository/Page_Centres  Services (Main)/centres_services_main_modal_edit-basic-information-tab"))

20
00:00:36,523 --> 00:00:37,309
63. setText(findTestObject("Object Repository/Page_Centres  Services (Main)/input_Hide Hero Image_centres_services_main_0b5310"), "Katalon Centres & Services edited")

21
00:00:37,310 --> 00:00:38,049
67. setText(findTestObject("Object Repository/Page_Centres  Services (Main)/input__centres_services_main_modal_edit-inp_68eb99"), "Katalon Centres & Services edited")

22
00:00:38,049 --> 00:00:39,107
71. setText(findTestObject("Object Repository/Page_Centres  Services (Main)/textarea__centres_services_main_modal_edit-_3296ff"), "This created by katalon, please dont perform anything to this content edited")

23
00:00:39,108 --> 00:00:39,858
75. setText(findTestObject("Object Repository/Page_Centres  Services (Main)/input_Display Find MP CTA_centres_services__230acb"), "Checked")

24
00:00:39,859 --> 00:00:40,511
79. setText(findTestObject("Object Repository/Page_Centres  Services (Main)/input_Additional CTA Label_centres_services_abe9f1"), "Checked")

25
00:00:40,512 --> 00:00:40,870
83. click(findTestObject("Object Repository/Page_Centres  Services (Main)/edit-meta-data-tab"))

26
00:00:40,870 --> 00:00:41,698
87. click(findTestObject("Page_Centres  Services (Main)/centres_services_main_modal_edit-button-saveAndClose"))

27
00:00:41,699 --> 00:00:41,833
91. closeBrowser()

