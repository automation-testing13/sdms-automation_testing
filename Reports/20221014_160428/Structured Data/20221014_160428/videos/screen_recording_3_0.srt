1
00:00:00,310 --> 00:00:02,419
1. openBrowser("")

2
00:00:02,477 --> 00:00:02,569
7. User must logged in.
To loginn refer to TC1.0 - User Authentication

3
00:00:02,582 --> 00:00:05,458
3. Enter url "https://uat.azlabs.sg/sdms"

4
00:00:05,458 --> 00:00:05,605
9. Redirecting to SSO Login page

5
00:00:05,605 --> 00:00:07,182
15. Verify SSO Login page reached

6
00:00:07,183 --> 00:00:08,339
21. Enter login username

7
00:00:08,340 --> 00:00:09,080
27. Enter login password

8
00:00:09,081 --> 00:00:11,586
33. Click login button

9
00:00:11,587 --> 00:00:12,877
13. SDMS landing page

10
00:00:12,877 --> 00:00:13,947
17. takeElementScreenshot(findTestObject("Object Repository/Page_Home/div_Structured Data"))

11
00:00:13,948 --> 00:00:14,478
23. Click " Structured Data" from left navigation bar

12
00:00:14,479 --> 00:00:15,888
27. takeElementScreenshot(findTestObject("Object Repository/Page_Home/a_Centres  Services (Main)"))

13
00:00:15,889 --> 00:00:17,806
33. Click  "Centres & Services (Main)" from child navigation bar

14
00:00:17,807 --> 00:00:20,682
37. waitForElementPresent(findTestObject("Object Repository/Page_Centres  Services (Main)/header_Centres  Services (Main)"), 1)

15
00:00:20,683 --> 00:00:22,686
43. List of "Centres & Services (Main)" list is shown in page

16
00:00:22,686 --> 00:00:23,130
47. click(findTestObject("Object Repository/Page_Centres  Services (Main)/button_Add Centres  Services (Main)"))

17
00:00:23,130 --> 00:00:24,043
51. waitForElementPresent(findTestObject("Object Repository/Page_Centres  Services (Main)/div_Add Centres  Services (Main)"), 1)

18
00:00:24,043 --> 00:00:25,574
57. List of "Centres & Services (Main)" list is shown in page

19
00:00:25,574 --> 00:00:26,116
61. click(findTestObject("Object Repository/Page_Centres  Services (Main)/centres_services_main_modal_add-basic-information-tab"))

20
00:00:26,116 --> 00:00:27,295
65. setText(findTestObject("Object Repository/Page_Centres  Services (Main)/input_Hide Hero Image_centres_services_main_dc649e"), "Katalon Centres & Services")

21
00:00:27,296 --> 00:00:28,131
69. setText(findTestObject("Object Repository/Page_Centres  Services (Main)/input__centres_services_main_modal_add-inpu_fdaa4b"), "Katalon Centres & Services")

22
00:00:28,131 --> 00:00:29,174
73. setText(findTestObject("Object Repository/Page_Centres  Services (Main)/textarea__centres_services_main_modal_add-t_3e4212"), "This created by katalon, please dont perform anything to this content")

23
00:00:29,174 --> 00:00:29,636
77. click(findTestObject("Object Repository/Page_Centres  Services (Main)/input_Display Enquiry CTA_centres_services__1b40fa"))

24
00:00:29,636 --> 00:00:29,991
81. click(findTestObject("Object Repository/Page_Centres  Services (Main)/input_Display Find MP CTA_centres_services__386d1d"))

25
00:00:29,992 --> 00:00:30,976
85. setText(findTestObject("Object Repository/Page_Centres  Services (Main)/input_Display Find MP CTA_centres_services__4218ae"), "Check")

26
00:00:30,976 --> 00:00:31,810
89. setText(findTestObject("Object Repository/Page_Centres  Services (Main)/input_Additional CTA Label_centres_services_ccf2cd"), "Check")

27
00:00:31,811 --> 00:00:32,208
93. click(findTestObject("Object Repository/Page_Centres  Services (Main)/div_Nothing selected"))

28
00:00:32,209 --> 00:00:32,752
97. click(findTestObject("Object Repository/Page_Centres  Services (Main)/a_Gleneagles Hospital"))

29
00:00:32,753 --> 00:00:34,111
101. selectOptionByValue(findTestObject("Object Repository/Page_Centres  Services (Main)/select_Gleneagles HospitalMount Elizabeth H_bd9683"), "2e3aee33-96d4-416c-8da6-1ff62e738955", true)

30
00:00:34,111 --> 00:00:35,120
105. selectOptionByValue(findTestObject("Object Repository/Page_Centres  Services (Main)/select_Gleneagles HospitalMount Elizabeth H_bd9683"), "2e3aee33-96d4-416c-8da6-1ff62e738955", true)

31
00:00:35,120 --> 00:00:36,129
109. selectOptionByValue(findTestObject("Object Repository/Page_Centres  Services (Main)/select_Gleneagles HospitalMount Elizabeth H_bd9683"), "2e3aee33-96d4-416c-8da6-1ff62e738955", true)

32
00:00:36,129 --> 00:00:37,138
113. selectOptionByValue(findTestObject("Object Repository/Page_Centres  Services (Main)/select_Gleneagles HospitalMount Elizabeth H_bd9683"), "2e3aee33-96d4-416c-8da6-1ff62e738955", true)

33
00:00:37,138 --> 00:00:37,694
117. click(findTestObject("Object Repository/Page_Centres  Services (Main)/button_Nothing selected"))

34
00:00:37,694 --> 00:00:38,244
121. click(findTestObject("Object Repository/Page_Centres  Services (Main)/span_Procedural Sedation_fa"))

35
00:00:38,244 --> 00:00:42,745
125. click(findTestObject("Object Repository/Page_Centres  Services (Main)/button_Done"))

36
00:00:42,746 --> 00:00:43,994
129. selectOptionByValue(findTestObject("Object Repository/Page_Centres  Services (Main)/select_No Listing                        By_393153"), "by_selection", true)

37
00:00:43,994 --> 00:00:44,523
133. click(findTestObject("Object Repository/Page_Centres  Services (Main)/div_Nothing selected"))

38
00:00:44,523 --> 00:00:45,740
137. selectOptionByValue(findTestObject("Object Repository/Page_Centres  Services (Main)/select_Clinical EmbryologyMedical AdvisorNu_bfe1e7"), "9d361e69-f422-4cb1-96c4-f6e71d8f2817", true)

39
00:00:45,740 --> 00:00:47,086
141. selectOptionByValue(findTestObject("Object Repository/Page_Centres  Services (Main)/select_Clinical EmbryologyMedical AdvisorNu_bfe1e7"), "9d361e69-f422-4cb1-96c4-f6e71d8f2817", true)

40
00:00:47,087 --> 00:00:48,333
145. selectOptionByValue(findTestObject("Object Repository/Page_Centres  Services (Main)/select_Clinical EmbryologyMedical AdvisorNu_bfe1e7"), "9d361e69-f422-4cb1-96c4-f6e71d8f2817", true)

41
00:00:48,333 --> 00:00:49,474
149. selectOptionByValue(findTestObject("Object Repository/Page_Centres  Services (Main)/select_Clinical EmbryologyMedical AdvisorNu_bfe1e7"), "9d361e69-f422-4cb1-96c4-f6e71d8f2817", true)

42
00:00:49,475 --> 00:00:50,817
153. selectOptionByValue(findTestObject("Object Repository/Page_Centres  Services (Main)/select_Clinical EmbryologyMedical AdvisorNu_bfe1e7"), "9d361e69-f422-4cb1-96c4-f6e71d8f2817", true)

43
00:00:50,818 --> 00:00:52,058
157. selectOptionByValue(findTestObject("Object Repository/Page_Centres  Services (Main)/select_Clinical EmbryologyMedical AdvisorNu_bfe1e7"), "9d361e69-f422-4cb1-96c4-f6e71d8f2817", true)

44
00:00:52,058 --> 00:00:53,429
161. selectOptionByValue(findTestObject("Object Repository/Page_Centres  Services (Main)/select_Clinical EmbryologyMedical AdvisorNu_bfe1e7"), "9d361e69-f422-4cb1-96c4-f6e71d8f2817", true)

45
00:00:53,430 --> 00:00:54,675
165. selectOptionByValue(findTestObject("Object Repository/Page_Centres  Services (Main)/select_Clinical EmbryologyMedical AdvisorNu_bfe1e7"), "9d361e69-f422-4cb1-96c4-f6e71d8f2817", true)

46
00:00:54,676 --> 00:00:55,848
169. selectOptionByValue(findTestObject("Object Repository/Page_Centres  Services (Main)/select_No Listing                        By_393153_1"), "by_selection", true)

47
00:00:55,848 --> 00:00:56,806
173. setText(findTestObject("Object Repository/Page_Centres  Services (Main)/input__centres_services_main_modal_add-inpu_420fda"), "1")

48
00:00:56,806 --> 00:00:58,381
179. List of "Centres & Services (Main)" list is shown in page

49
00:00:58,381 --> 00:00:58,935
183. click(findTestObject("Object Repository/Page_Centres  Services (Main)/centres_services_main_modal_add-meta-data-tab"))

50
00:00:58,935 --> 00:00:59,597
187. click(findTestObject("Object Repository/Page_Centres  Services (Main)/button_Add Meta Data"))

51
00:00:59,597 --> 00:01:00,693
191. setText(findTestObject("Object Repository/Page_Centres  Services (Main)/textarea_Social Summary_social-summary-md-8qisS"), "Katalon Centres And Services Metadata")

52
00:01:00,694 --> 00:01:01,165
195. click(findTestObject("Object Repository/Page_Centres  Services (Main)/button_Save"))

53
00:01:01,165 --> 00:01:03,155
199. deselectOptionByValue(findTestObject("Object Repository/Page_Centres  Services (Main)/select_Gleneagles HospitalMount Elizabeth H_bd9683"), "2e3aee33-96d4-416c-8da6-1ff62e738955", true)

54
00:01:03,155 --> 00:01:04,797
203. deselectOptionByValue(findTestObject("Object Repository/Page_Centres  Services (Main)/select_Clinical EmbryologyMedical AdvisorNu_bfe1e7"), "9d361e69-f422-4cb1-96c4-f6e71d8f2817", true)

55
00:01:04,797 --> 00:01:05,191
207. waitForElementPresent(findTestObject("Object Repository/Page_Centres  Services (Main)/h4_Successfully Created Centres  Services (Main)"), 1)

56
00:01:05,191 --> 00:01:05,372
211. closeBrowser()

