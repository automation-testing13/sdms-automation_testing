1
00:00:00,399 --> 00:00:00,402
3. Story: Pagination test on Centres & Services (Main) table

2
00:00:00,403 --> 00:00:00,473
9. User must logged in.
For login refer TC1.0 - User Authentication.

3
00:00:00,474 --> 00:00:03,330
3. Enter url "https://uat.azlabs.sg/sdms"

4
00:00:03,331 --> 00:00:03,528
9. Redirecting to SSO Login page

5
00:00:03,528 --> 00:00:05,012
15. Verify SSO Login page reached

6
00:00:05,012 --> 00:00:06,215
21. Enter login username

7
00:00:06,215 --> 00:00:06,967
27. Enter login password

8
00:00:06,968 --> 00:00:09,159
33. Click login button

9
00:00:09,159 --> 00:00:10,484
15. SDMS landing page

10
00:00:10,484 --> 00:00:11,576
21. Structured Data navigation menu

11
00:00:11,576 --> 00:00:12,108
27. Click " Structured Data" from left navigation bar

12
00:00:12,108 --> 00:00:13,372
33. Centres & Services (Main) navigation menu

13
00:00:13,372 --> 00:00:15,246
39. Click  "Centres & Services (Main)" from child navigation bar

14
00:00:15,246 --> 00:00:19,532
45. List of "Centres & Services (Main)" list is shown in page

15
00:00:19,533 --> 00:00:19,534
49. if (true)

16
00:00:19,534 --> 00:00:20,064
1. verifyElementClickable(findTestObject("Page_Centres  Services (Main)/a_Next"))

17
00:00:20,065 --> 00:00:21,327
7. Next button

18
00:00:21,328 --> 00:00:21,843
13. Click "Next" to go to next page

19
00:00:21,844 --> 00:00:21,884
53. if (true)

20
00:00:21,885 --> 00:00:22,300
1. verifyElementClickable(findTestObject("Page_Centres  Services (Main)/a_Previous"))

21
00:00:22,301 --> 00:00:23,605
7. Previous button

22
00:00:23,606 --> 00:00:24,160
13. Click "Previous" to back to previous page

23
00:00:24,160 --> 00:00:24,348
57. closeBrowser()

