1
00:00:00,652 --> 00:00:00,737
3. Story: Get list of Centres & Services (Main)

2
00:00:00,748 --> 00:00:01,195
9. User must logged in.
To login refer to TC1.0 - User Authentication.

3
00:00:01,195 --> 00:00:05,953
3. Enter url "https://uat.azlabs.sg/sdms"

4
00:00:05,953 --> 00:00:06,210
9. Redirecting to SSO Login page

5
00:00:06,212 --> 00:00:07,911
15. Verify SSO Login page reached

6
00:00:07,931 --> 00:00:09,321
21. Enter login username

7
00:00:09,322 --> 00:00:10,209
27. Enter login password

8
00:00:10,209 --> 00:00:12,720
33. Click login button

9
00:00:12,720 --> 00:00:14,206
15. SDMS landing page

10
00:00:14,206 --> 00:00:15,404
21. Structured Data button menu

11
00:00:15,405 --> 00:00:15,778
27. Click " Structured Data" from left navigation bar

12
00:00:15,778 --> 00:00:17,071
33. Centres & Services (Main) button menu

13
00:00:17,071 --> 00:00:28,086
39. Click  "Centres & Services (Main)" from child navigation bar

14
00:00:28,087 --> 00:00:30,898
43. waitForElementPresent(findTestObject("Object Repository/Page_Centres  Services (Main)/header_Centres  Services (Main)"), 1)

15
00:00:30,898 --> 00:00:32,704
49. List of "Centres & Services (Main)" list is shown in page

16
00:00:32,705 --> 00:00:32,852
53. closeBrowser()

