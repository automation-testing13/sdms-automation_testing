import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Enter url "https://uat.azlabs.sg/sdms"'
WebUI.openBrowser(GlobalVariable.BaseUrl)

'Redirecting to SSO Login page'
WebUI.navigateToUrl('https://gluu-ppl.azlabs.sg/oxauth/login.htm')

'Verify SSO Login page reached'
WebUI.waitForElementNotVisible(findTestObject('Object Repository/Page_Parkway Pantai - Login/div_circle-loader'), 1)

'Enter login username'
WebUI.setText(findTestObject('Object Repository/Page_Parkway Pantai - Login/loginForm_username'), GlobalVariable.Username)

'Enter login password'
WebUI.setText(findTestObject('Object Repository/Page_Parkway Pantai - Login/loginForm_password'), GlobalVariable.Password)

'Click login button'
WebUI.click(findTestObject('Object Repository/Page_Parkway Pantai - Login/button_Login'))

