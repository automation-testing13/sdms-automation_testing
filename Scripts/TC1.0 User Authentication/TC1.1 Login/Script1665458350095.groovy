import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Story: login into SDMS System'
WebUI.comment('')

'Given that user has valid login information'
WebUI.comment('')

'Enter SDMS Admin Portal url '
WebUI.openBrowser(GlobalVariable.BaseUrl)

'Redirecting to SSO Login page'
WebUI.navigateToUrl(GlobalVariable.SsoUrl)

'Verify SSO Login page reached'
WebUI.waitForElementNotVisible(findTestObject('Object Repository/Page_Parkway Pantai - Login/div_circle-loader'), 1)

'SSO Landing Page shown. Fill in username and password'
WebUI.takeFullPageScreenshotAsCheckpoint('login-page')

'Enter login username'
WebUI.setText(findTestObject('Object Repository/Page_Parkway Pantai - Login/loginForm_username'), GlobalVariable.Username)

'username form field'
WebUI.takeElementScreenshot(findTestObject('Object Repository/Page_Parkway Pantai - Login/loginForm_username'))

'Enter login password'
WebUI.setText(findTestObject('Object Repository/Page_Parkway Pantai - Login/loginForm_password'), GlobalVariable.Password)

'password form field'
WebUI.takeElementScreenshot(findTestObject('Object Repository/Page_Parkway Pantai - Login/loginForm_password'))

'Filled username and password'
WebUI.takeFullPageScreenshotAsCheckpoint('filled-login-page')

'Login button'
WebUI.takeElementScreenshot(findTestObject('Object Repository/Page_Parkway Pantai - Login/button_Login'))

'Click login button'
WebUI.click(findTestObject('Object Repository/Page_Parkway Pantai - Login/button_Login'))

'Then user should be able to login'
WebUI.comment('')

'Page redirected to SDMS landing page'
WebUI.takeFullPageScreenshotAsCheckpoint('landing-page')

'Finish'
WebUI.closeBrowser()

