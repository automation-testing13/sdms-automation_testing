import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Story: View details of Centres & Services (Main)'
WebUI.comment('')

'Refer TC1.0 - User Authentication'
WebUI.callTestCase(findTestCase('Authentication'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_Home/div_Structured Data                        _67d0e0'))

WebUI.click(findTestObject('Object Repository/Page_Home/a_Centres  Services (Main)'))

WebUI.setText(findTestObject('Object Repository/Page_Centres  Services (Main)/input_Search_centres_services_main-input-search'), 
    'katalon')

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/button_Search'))

WebUI.setText(findTestObject('Object Repository/Page_Centres  Services (Main)/input_Search_centres_services_main-input-search'), 
    'katalon')

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/button_Draft_centres_services_main-button-t_b71ae3'))

WebUI.waitForElementPresent(findTestObject('Object Repository/Page_Centres  Services (Main)/div_Centres  Services (Main)               _81135e'), 
    0)

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/centres_services_main_modal_translate-meta-data-tab'))

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/centres_services_main_modal_translate-basic-information-tab'))

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/button_'))

WebUI.closeBrowser()

