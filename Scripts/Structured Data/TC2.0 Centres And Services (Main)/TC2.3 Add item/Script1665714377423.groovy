import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'User must logged in.\nTo loginn refer to TC1.0 - User Authentication'
WebUI.callTestCase(findTestCase('Authentication'), [:], FailureHandling.STOP_ON_FAILURE)

'SDMS landing page'
WebUI.takeFullPageScreenshotAsCheckpoint('landing-page')

WebUI.takeElementScreenshot(findTestObject('Object Repository/Page_Home/div_Structured Data'))

'Click " Structured Data" from left navigation bar'
WebUI.click(findTestObject('Object Repository/Page_Home/div_Structured Data'))

WebUI.takeElementScreenshot(findTestObject('Object Repository/Page_Home/a_Centres  Services (Main)'))

'Click  "Centres & Services (Main)" from child navigation bar'
WebUI.click(findTestObject('Object Repository/Page_Home/a_Centres  Services (Main)'))

WebUI.waitForElementPresent(findTestObject('Object Repository/Page_Centres  Services (Main)/header_Centres  Services (Main)'), 
    1)

'List of "Centres & Services (Main)" list is shown in page'
WebUI.takeFullPageScreenshotAsCheckpoint('centres&services-main-list')

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/button_Add Centres  Services (Main)'))

WebUI.waitForElementPresent(findTestObject('Object Repository/Page_Centres  Services (Main)/div_Add Centres  Services (Main)'), 
    1)

'List of "Centres & Services (Main)" list is shown in page'
WebUI.takeFullPageScreenshotAsCheckpoint('centres_services_main_modal_add')

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/centres_services_main_modal_add-basic-information-tab'))

WebUI.setText(findTestObject('Object Repository/Page_Centres  Services (Main)/input_Hide Hero Image_centres_services_main_dc649e'), 
    'Katalon Centres & Services')

WebUI.setText(findTestObject('Object Repository/Page_Centres  Services (Main)/input__centres_services_main_modal_add-inpu_fdaa4b'), 
    'Katalon Centres & Services')

WebUI.setText(findTestObject('Object Repository/Page_Centres  Services (Main)/textarea__centres_services_main_modal_add-t_3e4212'), 
    'This created by katalon, please dont perform anything to this content')

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/input_Display Enquiry CTA_centres_services__1b40fa'))

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/input_Display Find MP CTA_centres_services__386d1d'))

WebUI.setText(findTestObject('Object Repository/Page_Centres  Services (Main)/input_Display Find MP CTA_centres_services__4218ae'), 
    'Check')

WebUI.setText(findTestObject('Object Repository/Page_Centres  Services (Main)/input_Additional CTA Label_centres_services_ccf2cd'), 
    'Check')

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/div_Nothing selected'))

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/a_Gleneagles Hospital'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Centres  Services (Main)/select_Gleneagles HospitalMount Elizabeth H_bd9683'), 
    '2e3aee33-96d4-416c-8da6-1ff62e738955', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Centres  Services (Main)/select_Gleneagles HospitalMount Elizabeth H_bd9683'), 
    '2e3aee33-96d4-416c-8da6-1ff62e738955', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Centres  Services (Main)/select_Gleneagles HospitalMount Elizabeth H_bd9683'), 
    '2e3aee33-96d4-416c-8da6-1ff62e738955', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Centres  Services (Main)/select_Gleneagles HospitalMount Elizabeth H_bd9683'), 
    '2e3aee33-96d4-416c-8da6-1ff62e738955', true)

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/button_Nothing selected'))

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/span_Procedural Sedation_fa'))

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/button_Done'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Centres  Services (Main)/select_No Listing                        By_393153'), 
    'by_selection', true)

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/div_Nothing selected'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Centres  Services (Main)/select_Clinical EmbryologyMedical AdvisorNu_bfe1e7'), 
    '9d361e69-f422-4cb1-96c4-f6e71d8f2817', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Centres  Services (Main)/select_Clinical EmbryologyMedical AdvisorNu_bfe1e7'), 
    '9d361e69-f422-4cb1-96c4-f6e71d8f2817', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Centres  Services (Main)/select_Clinical EmbryologyMedical AdvisorNu_bfe1e7'), 
    '9d361e69-f422-4cb1-96c4-f6e71d8f2817', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Centres  Services (Main)/select_Clinical EmbryologyMedical AdvisorNu_bfe1e7'), 
    '9d361e69-f422-4cb1-96c4-f6e71d8f2817', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Centres  Services (Main)/select_Clinical EmbryologyMedical AdvisorNu_bfe1e7'), 
    '9d361e69-f422-4cb1-96c4-f6e71d8f2817', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Centres  Services (Main)/select_Clinical EmbryologyMedical AdvisorNu_bfe1e7'), 
    '9d361e69-f422-4cb1-96c4-f6e71d8f2817', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Centres  Services (Main)/select_Clinical EmbryologyMedical AdvisorNu_bfe1e7'), 
    '9d361e69-f422-4cb1-96c4-f6e71d8f2817', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Centres  Services (Main)/select_Clinical EmbryologyMedical AdvisorNu_bfe1e7'), 
    '9d361e69-f422-4cb1-96c4-f6e71d8f2817', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Centres  Services (Main)/select_No Listing                        By_393153_1'), 
    'by_selection', true)

WebUI.setText(findTestObject('Object Repository/Page_Centres  Services (Main)/input__centres_services_main_modal_add-inpu_420fda'), 
    '1')

'List of "Centres & Services (Main)" list is shown in page'
WebUI.takeFullPageScreenshotAsCheckpoint('centres_services_main_modal_add-meta-data-tab')

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/centres_services_main_modal_add-meta-data-tab'))

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/button_Add Meta Data'))

WebUI.setText(findTestObject('Object Repository/Page_Centres  Services (Main)/textarea_Social Summary_social-summary-md-8qisS'), 
    'Katalon Centres And Services Metadata')

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/button_Save'))

WebUI.deselectOptionByValue(findTestObject('Object Repository/Page_Centres  Services (Main)/select_Gleneagles HospitalMount Elizabeth H_bd9683'), 
    '2e3aee33-96d4-416c-8da6-1ff62e738955', true)

WebUI.deselectOptionByValue(findTestObject('Object Repository/Page_Centres  Services (Main)/select_Clinical EmbryologyMedical AdvisorNu_bfe1e7'), 
    '9d361e69-f422-4cb1-96c4-f6e71d8f2817', true)

WebUI.waitForElementPresent(findTestObject('Object Repository/Page_Centres  Services (Main)/h4_Successfully Created Centres  Services (Main)'), 
    1)

WebUI.closeBrowser()

