import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Story: Get list of Centres & Services (Main)'
WebUI.comment('')

'User must logged in.\nTo login refer to TC1.0 - User Authentication.'
WebUI.callTestCase(findTestCase('Authentication'), [:], FailureHandling.STOP_ON_FAILURE)

'SDMS landing page'
WebUI.takeFullPageScreenshotAsCheckpoint('landing-page')

'Structured Data button menu'
WebUI.takeElementScreenshot(findTestObject('Page_Home/div_Structured Data'))

'Click " Structured Data" from left navigation bar'
WebUI.click(findTestObject('Page_Home/div_Structured Data'))

'Centres & Services (Main) button menu'
WebUI.takeElementScreenshot(findTestObject('Page_Home/a_Centres  Services (Main)'))

'Click  "Centres & Services (Main)" from child navigation bar'
WebUI.click(findTestObject('Page_Home/a_Centres  Services (Main)'))

WebUI.waitForElementPresent(findTestObject('Object Repository/Page_Centres  Services (Main)/header_Centres  Services (Main)'), 
    1)

'List of "Centres & Services (Main)" list is shown in page'
WebUI.takeFullPageScreenshotAsCheckpoint('centres&services-main-list')

WebUI.closeBrowser()

