import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'User must logged in.\nTo loginn refer to TC1.0 - User Authentication'
WebUI.callTestCase(findTestCase('Authentication'), [:], FailureHandling.STOP_ON_FAILURE)

'SDMS landing page'
WebUI.takeFullPageScreenshotAsCheckpoint('landing-page')

WebUI.takeElementScreenshot(findTestObject('Object Repository/Page_Home old/div_Structured Data                        _67d0e0'))

'Click " Structured Data" from left navigation bar'
WebUI.click(findTestObject('Object Repository/Page_Home old/div_Structured Data                        _67d0e0'))

WebUI.takeElementScreenshot(findTestObject('Object Repository/Page_Home old/span_Centres  Services (Main)'))

'Click  "Centres & Services (Main)" from child navigation bar'
WebUI.click(findTestObject('Object Repository/Page_Home old/span_Centres  Services (Main)'))

'List of "Centres & Services (Main)" list is shown in page'
WebUI.takeFullPageScreenshotAsCheckpoint('centres&services-main-list')

WebUI.setText(findTestObject('Object Repository/Page_Centres  Services (Main)/input_Search_centres_services_main-input-search'), 
    'katalon')

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/button_Search'))

WebUI.setText(findTestObject('Object Repository/Page_Centres  Services (Main)/input_Search_centres_services_main-input-search'), 
    'katalon')

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/button_Draft_centres_services_main-button-edit'))

WebUI.waitForElementPresent(findTestObject('Object Repository/Page_Centres  Services (Main)/div_Edit Centres  Services (Main)          _24fcef'), 
    1)

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/centres_services_main_modal_edit-basic-information-tab'))

WebUI.setText(findTestObject('Object Repository/Page_Centres  Services (Main)/input_Hide Hero Image_centres_services_main_0b5310'), 
    'Katalon Centres & Services edited')

WebUI.setText(findTestObject('Object Repository/Page_Centres  Services (Main)/input__centres_services_main_modal_edit-inp_68eb99'), 
    'Katalon Centres & Services edited')

WebUI.setText(findTestObject('Object Repository/Page_Centres  Services (Main)/textarea__centres_services_main_modal_edit-_3296ff'), 
    'This created by katalon, please dont perform anything to this content edited')

WebUI.setText(findTestObject('Object Repository/Page_Centres  Services (Main)/input_Display Find MP CTA_centres_services__230acb'), 
    'Checked')

WebUI.setText(findTestObject('Object Repository/Page_Centres  Services (Main)/input_Additional CTA Label_centres_services_abe9f1'), 
    'Checked')

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/edit-meta-data-tab'))

WebUI.click(findTestObject('Page_Centres  Services (Main)/centres_services_main_modal_edit-button-saveAndClose'))

WebUI.closeBrowser()

