import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Story: Pagination test on Centres & Services (Main) table'
WebUI.comment('')

'User must logged in.\nFor login refer TC1.0 - User Authentication.'
WebUI.callTestCase(findTestCase('Authentication'), [:], FailureHandling.STOP_ON_FAILURE)

'SDMS landing page'
WebUI.takeFullPageScreenshotAsCheckpoint('landing-page')

'Structured Data navigation menu'
WebUI.takeElementScreenshot(findTestObject('Page_Home/div_Structured Data'))

'Click " Structured Data" from left navigation bar'
WebUI.click(findTestObject('Page_Home/div_Structured Data'))

'Centres & Services (Main) navigation menu'
WebUI.takeElementScreenshot(findTestObject('Page_Home/a_Centres  Services (Main)'))

'Click  "Centres & Services (Main)" from child navigation bar'
WebUI.click(findTestObject('Page_Home/a_Centres  Services (Main)'))

'List of "Centres & Services (Main)" list is shown in page'
WebUI.takeFullPageScreenshotAsCheckpoint('centres&services-main-list')

if (true) {
    WebUI.verifyElementClickable(findTestObject('Page_Centres  Services (Main)/a_Next'))

    'Next button'
    WebUI.takeElementScreenshot(findTestObject('Page_Centres  Services (Main)/a_Next'))

    'Click "Next" to go to next page'
    WebUI.click(findTestObject('Page_Centres  Services (Main)/a_Next'))
}

if (true) {
    WebUI.verifyElementClickable(findTestObject('Page_Centres  Services (Main)/a_Previous'))

    'Previous button'
    WebUI.takeElementScreenshot(findTestObject('Page_Centres  Services (Main)/a_Previous'))

    'Click "Previous" to back to previous page'
    WebUI.click(findTestObject('Page_Centres  Services (Main)/a_Previous'))
}

WebUI.closeBrowser()

