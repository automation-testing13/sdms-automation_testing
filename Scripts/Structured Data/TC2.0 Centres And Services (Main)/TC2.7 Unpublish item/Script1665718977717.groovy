import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'User must logged in.\nTo loginn refer to TC1.0 - User Authentication'
WebUI.callTestCase(findTestCase('Authentication'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Home/div_Structured Data Approval               _3b52a2'))

WebUI.click(findTestObject('Object Repository/Page_Home/a_Centres  Services (Main)'))

WebUI.setText(findTestObject('Object Repository/Page_Centres  Services (Main) Approvals/input_Search_dtSearchInput'), 'katalon')

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main) Approvals/button_Search'))

WebUI.setText(findTestObject('Object Repository/Page_Centres  Services (Main) Approvals/input_Search_dtSearchInput'), 'katalon')

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main) Approvals/button_Review Data_mat-button btn-only-icon_4481e8'))

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main) Approvals/button_Approve'))

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main) Approvals/h4_Successfully Approve Centres  Services (Main)'))

WebUI.waitForElementPresent(findTestObject('Object Repository/Page_Centres  Services (Main) Approvals/h4_Successfully Approve Centres  Services (Main)'), 
    0)

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main) Approvals/div_Structured Data                        _67d0e0'))

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main) Approvals/a_Centres  Services (Main)'))

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/div_Search'))

WebUI.setText(findTestObject('Page_Centres  Services (Main)/input_Search_centres_services_main-input-search'), 'katalon')

WebUI.click(findTestObject('Page_Centres  Services (Main)/button_Search'))

WebUI.setText(findTestObject('Page_Centres  Services (Main)/input_Search_centres_services_main-input-search'), 'katalon')

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/centres_services_main-button-unpublish'))

WebUI.click(findTestObject('Object Repository/Page_Centres  Services (Main)/centres_services_main_modal_unpublish-button-unpublish'))

WebUI.waitForElementPresent(findTestObject('Object Repository/Page_Centres  Services (Main)/h4_Successfully Unpublish Centres  Services (Main)'), 
    0)

WebUI.closeBrowser()

